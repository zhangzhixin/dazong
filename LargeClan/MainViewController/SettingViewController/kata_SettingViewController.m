//
//  kata_SettingViewController.m
//  LargeClan
//
//  Created by kata on 13-11-19.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import "kata_SettingViewController.h"
#import "KATAUtils.h"

#import "kata_AboutWeViewController.h"
#import "kata_HelpViewController.h"
#import "kata_feedBackViewController.h"

#import <ShareSDK/ShareSDK.h>
@interface kata_SettingViewController ()


@property(nonatomic,retain)NSArray *array;
@end

@implementation kata_SettingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.title = @"设置";
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"header.png"] forBarMetrics:UIBarMetricsDefault];

    
   
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight) style:UITableViewStyleGrouped];
    
    tableView.backgroundView = nil;
        
    tableView.delegate = self;
    
    
    tableView.dataSource  = self;
    
    [self.view addSubview:tableView];
    

    self.array = [[[NSArray alloc]initWithObjects:@"使用帮助",@"用户反馈",@"客服热线",@"关于我们", nil] autorelease];
    
    

}

#pragma mark - tableViewDataSoure


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (section ==2)
    {
        return 4;
    }
    else
    {
        return 1;
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   static NSString *cellIdentifier = @"CellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell)
    {
        
        cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
        
    
    }
    
    switch (indexPath.section)
    {
        case 0:
        {
            cell.textLabel.text = @"分享软件";
        }
            break;
        case 1:
        {
            cell.textLabel.text = @"检查更新";

        }
            break;
        case 2:
        {
            
            cell.textLabel.text = [self.array objectAtIndex:indexPath.row];
            
            
        }
            break;
            
        default:
            break;
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
    
}

#pragma mark - tableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        
        
        
        //构造分享内容
        id<ISSContent> publishContent = [ShareSDK content:@"分享内容"
                                           defaultContent:@"默认分享内容，没内容时显示"
                                                    image:nil
                                                    title:@"ShareSDK"
                                                      url:@"http://www.sharesdk.cn"
                                              description:@"这是一条测试信息"
                                                mediaType:SSPublishContentMediaTypeText];
        
        [ShareSDK showShareActionSheet:nil
                             shareList:nil
                               content:publishContent
                         statusBarTips:YES
                           authOptions:nil
                          shareOptions: nil
                                result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                    if (state == SSResponseStateSuccess)
                                    {
                                        NSLog(@"分享成功");
                                    }
                                    else if (state == SSResponseStateFail)
                                    {
                                        NSLog(@"分享失败,错误码:%d,错误描述:%@", [error errorCode], [error errorDescription]);
                                    }
                                }];

//        NSString *imagePath = [[NSBundle mainBundle] pathForResource:@"ShareSDK"  ofType:@"jpg"];
//        
//        //构造分享内容
//        id<ISSContent> publishContent = [ShareSDK content:@"分享内容"
//                                           defaultContent:@"默认分享内容，没内容时显示"
//                                                    image:[ShareSDK imageWithPath:imagePath]
//                                                    title:@"ShareSDK"
//                                                      url:@"http://www.sharesdk.cn"
//                                              description:@"这是一条测试信息"
//                                                mediaType:SSPublishContentMediaTypeNews];
//        
//        [ShareSDK showShareActionSheet:nil
//                             shareList:nil
//                               content:publishContent
//                         statusBarTips:YES
//                           authOptions:nil
//                          shareOptions: nil
//                                result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
//                                    if (state == SSResponseStateSuccess)
//                                    {
//                                        NSLog(@"分享成功");
//                                    }
//                                    else if (state == SSResponseStateFail)
//                                    {
//                                        NSLog(@"分享失败,错误码:%d,错误描述:%@", [error errorCode], [error errorDescription]);
//                                    }
//                                }];
        
    }
    
    
    // 使用帮助
    if (indexPath.section == 2 && indexPath.row == 0) {
        
        kata_HelpViewController *helpVC = [[kata_HelpViewController alloc]init];
        
        [self.navigationController pushViewController:helpVC animated:YES];
        
        [helpVC release];
    }
    // 用户反馈
    if (indexPath.section == 2 && indexPath.row == 1) {
        
        kata_feedBackViewController *feedBackVC  = [[kata_feedBackViewController alloc]init];
        
        [self.navigationController pushViewController:feedBackVC animated:YES];
        
        
        [feedBackVC release];
        
        
    
    }

    
    // 客服热线
    if (indexPath.section == 2 && indexPath.row == 2) {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"客服热线" message:@"0571-82856333" delegate:self cancelButtonTitle:@"拨打" otherButtonTitles:@"取消", nil];
        
        
        [alert show];
        
        [alert release];
        
        
    }
    

    //关于我们
    if (indexPath.section == 2 && indexPath.row == 3)  {
        
        kata_AboutWeViewController *aboutVC = [[kata_AboutWeViewController alloc]init];
        
        [self.navigationController pushViewController:aboutVC animated:YES];
        
        [aboutVC release];
    }

    
    
    
}

#pragma mark - alertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    
    
    
}







- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
