//
//  kata_SearchViewController.m
//  LargeClan
//
//  Created by kata on 13-11-19.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import "kata_SearchViewController.h"
#import "KATAUtils.h"
#import "kata_segView.h"
#import "kata_Tools.h"
#import "kata_AddressViewController.h"
#import "KATAUtils.h"
#import "kata_DeliveryTimeCell.h"

#import "kata_QueryResultViewController.h"

#import "kata_SearchResultViewController.h"
@interface kata_SelcetConditionCell : UITableViewCell

@end

@implementation kata_SelcetConditionCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        
        
        UILabel *nameLab  = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 44)];
        
        nameLab.tag = 11;
        
        nameLab.backgroundColor = [UIColor clearColor];
        
        nameLab.backgroundColor = [UIColor redColor];
        
        [self.contentView addSubview:nameLab];
        
        [nameLab release];
        
        UITextField *textFiled = [[UITextField alloc]initWithFrame:CGRectMake(90, 7, 220, 30)];
        
        textFiled.userInteractionEnabled = NO;
        textFiled.tag = 12;
        
        textFiled.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        
        textFiled.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        
//        textFiled.backgroundColor = [UIColor redColor];
        
        [self.contentView addSubview:textFiled];
        
        [textFiled release];
 
    }
    
    return self;
    
}

@end



@interface kata_SearchViewController ()
{
    UITableView *_tableView;
    
    
    UIDatePicker *_picker;

    BOOL isPicker;
    
    
    int _plateType;
    
    
//    UITextField *_textField;
}
@property(nonatomic,retain)NSMutableArray *dataArray;

@property(nonatomic,retain)NSString *priceAtAalertStr;

@property(nonatomic,retain)NSString *dirStr;
@property(nonatomic,retain)NSString *priceStr;
@property(nonatomic,retain)NSString *goodsStr;
@property(nonatomic,retain)NSString *shipStr;
@property(nonatomic,retain)NSString *arrivalStr;
@property(nonatomic,retain)NSString *stateStr;
@property(nonatomic,retain)NSString *addressStr;


@property(nonatomic,retain)NSString *dateStr1;
@property(nonatomic,retain)NSString *dateStr2;

@property(nonatomic,retain)NSString *delivelyTimeStr;

@end

@implementation kata_SearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        
    }
    return self;
}
-(void)dealloc
{
    [_picker release];

    [_dataArray release];

    [super dealloc];
}
#pragma mark - beginningView

-(void)beginningView
{
    self.title = @"搜索";
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"header.png"] forBarMetrics:UIBarMetricsDefault];
    
    
    
    self.dataArray = [NSMutableArray arrayWithObjects:@"方向",@"价格",@"货种",@"交割时间",@"状态",@"交割地", nil];
    
    kata_segView *segView = [[kata_segView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 40)];
    
    DebugLog(@"%@",NSStringFromCGRect(segView.frame));
    
    segView.delegate = self;
    
    [self.view addSubview:segView];
    
    [segView release];

    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self beginningView];
    
//    UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 40, ScreenWidth, ScreenHeight)];
    
    
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 40, ScreenWidth, ScreenHeight) style:UITableViewStyleGrouped];
    
    
    _tableView.backgroundView = nil;
    
    _tableView.delegate = self;
    
    _tableView.dataSource = self;
    
    [self.view addSubview:_tableView];
    

}

#pragma mark - kata_segDelegate
-(void)changeNameData:(NSMutableArray *)dataArray;
{
    DebugLog(@"代理");
    
    [self.dataArray removeAllObjects];
    

    [self.dataArray addObjectsFromArray:dataArray];
    
    DebugLog(@"self.dataArray == %d",[self.dataArray count]);
    
    [_tableView reloadData];
    
}

- (void)changeIndex:(int)index
{
    
    _plateType = index - 1;
    
    
}

#pragma mark - custom
-(void)queryBtnClick
{
    DebugLog(@"查询");
    
//    kata_SearchResultViewController *searchResult = [[kata_SearchResultViewController alloc]init];
//    
//    [self.navigationController pushViewController:searchResult animated:YES];
//    
//    [searchResult release];
//    
    
    kata_QueryResultViewController *queryResultVC = [[kata_QueryResultViewController alloc]init];
    
    [self.navigationController pushViewController:queryResultVC animated:YES];
    
    [queryResultVC release];
    
    
    
}
- (void)changeTime:(id)sender
{
    
    UIDatePicker *picker = (UIDatePicker *)sender;
    
    isPicker =  NO;
    
    [UIView beginAnimations:nil context:nil];
    
    [UIView setAnimationDuration:1];
    
    
    _picker.frame = CGRectMake(0, ScreenHeight + 216, ScreenWidth, 216);
    
    [UIView commitAnimations];
    
    
//    UIDatePicker* control = (UIDatePicker*)sender;
    
    NSDate* _date = _picker.date;
    
    DebugLog(@"_date == %@",_date);
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];

    NSString *dateStr = [formatter stringFromDate:_date];
    
    if (picker.tag ==1) {
        self.dateStr1 = dateStr;
    }else if (picker.tag == 2){
       self.dateStr2 = dateStr;

    }
    else{
        self.delivelyTimeStr = dateStr;
        
    }

    
    [formatter release];

    [_tableView reloadData];
}

#pragma mark - tableViewDataSoure
-(float)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 85.0;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    
    
    UIView *view = [[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 85.0)] autorelease];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    button.frame = CGRectMake(85, 20, 150, 45);
    
    [button addTarget:self action:@selector(queryBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    [button setImage:[UIImage imageNamed:@"but.png"] forState:UIControlStateNormal];
    
    [view addSubview:button];
    
    
    return view;
    
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [self.dataArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    static  NSString *cellIdtifierMEGInside = @"CellMEGInside ";
    
    static  NSString *cellIdtifierMEGInsideTime = @"CellMEGInsideTime";
    
    static  NSString *cellIdtifierMEGOut = @"CellMEGOut";
    
    static  NSString *cellIdtifierMEGOutTime  = @"CellMEGOutTime";

    static  NSString *cellIdtifierPTAInside = @"CellPTAInside";
    
    static  NSString *cellIdtifierPTAInsideTime  = @"CellPTAInsideTime ";

    static  NSString *cellIdtifierPTAOut = @"CelPTAOut";
    
    static  NSString *cellIdtifierPTAOutTime = @"CelPTAOutTime";

    int row = indexPath.row;

    if (_plateType == 0) {   //MEG内盘
        
        if (row == 3) {
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdtifierMEGInsideTime];
            
            if (!cell) {
                
                cell = [[[kata_DeliveryTimeCell   alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdtifierMEGInsideTime] autorelease];
                
                
                
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell;
            
        }else{
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdtifierMEGInside];

            
            if (!cell) {
                
                cell = [[[kata_SelcetConditionCell   alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdtifierMEGInside] autorelease];
                
                
                
            }
            
            UILabel *nameLab  = (UILabel *)[cell.contentView viewWithTag:11];

            nameLab.text = [self.dataArray objectAtIndex:indexPath.row];

            if (indexPath.row == ([self.dataArray count] - 1) )
            {

                cell.accessoryType   = UITableViewCellAccessoryDisclosureIndicator;

            }

            UITextField *textFiled = (UITextField *)[cell.contentView viewWithTag:12];

            textFiled.text = [self.dataArray objectAtIndex:indexPath.row];


            if ([textFiled.text isEqualToString:@"方向"])
            {
                textFiled.text = self.dirStr;
            }
            if ([textFiled.text isEqualToString:@"价格"])
            {
                textFiled.text = self.priceAtAalertStr;
            }
            if ([textFiled.text isEqualToString:@"货种"])
            {
                textFiled.text = self.goodsStr;
            }
            if ([textFiled.text isEqualToString:@"交割时间"])
            {
                textFiled.text = nil;
            }
            if ([textFiled.text isEqualToString:@"状态"])
            {
                textFiled.text = self.stateStr;
            }
            if ([textFiled.text isEqualToString:@"交割地"])
            {
                
                textFiled.text = self.addressStr;
            }

            
            
            
            
            
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell;

            
            
        }
        
        
        
        
    }else if(_plateType == 1){ //MEG外盘
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdtifierMEGOut];


            if (!cell) {


               cell = [[[kata_SelcetConditionCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdtifierMEGOutTime] autorelease];

            }
        
                

//        if (row == 3 || row == 4) {
//            
//            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdtifierMEGOutTime];
//            
//            if (!cell) {
//                
//                cell = [[[kata_DeliveryTimeCell   alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdtifierMEGOutTime] autorelease];
//                
//                
//                
//            }
//            
//            cell.selectionStyle = UITableViewCellSelectionStyleNone;
//            
//            return cell;
//            
//        }else{
//            
//            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdtifierMEGOut];
//            
//            
//            if (!cell) {
//                
//                cell = [[[kata_SelcetConditionCell   alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdtifierMEGOut] autorelease];
//                
//                
//                
//            }
        
            UILabel *nameLab  = (UILabel *)[cell.contentView viewWithTag:11];
            
            nameLab.text = [self.dataArray objectAtIndex:indexPath.row];
            
            if (indexPath.row == ([self.dataArray count] - 1) )
            {
                
                cell.accessoryType   = UITableViewCellAccessoryDisclosureIndicator;
                
            }
            
            UITextField *textFiled = (UITextField *)[cell.contentView viewWithTag:12];
            
            textFiled.text = [self.dataArray objectAtIndex:indexPath.row];
        
        
            if ([textFiled.text isEqualToString:@"装船时间"])
            {
                textFiled.text = self.dateStr1;
            }
            if ([textFiled.text isEqualToString:@"到港时间"])
            {
                textFiled.text = self.dateStr2;
            }

        
            if ([textFiled.text isEqualToString:@"方向"])
            {
                textFiled.text = self.dirStr;
            }
            if ([textFiled.text isEqualToString:@"价格"])
            {
                textFiled.text = self.priceAtAalertStr;
            }
            if ([textFiled.text isEqualToString:@"类型"])
            {
                textFiled.text = self.shipStr;
            }
            if ([textFiled.text isEqualToString:@"交割时间"])
            {
                textFiled.text = nil;
            }
            if ([textFiled.text isEqualToString:@"状态"])
            {
                textFiled.text = self.stateStr;
            }
            if ([textFiled.text isEqualToString:@"交割地"])
            {
                
                textFiled.text = self.addressStr;
            }

            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell;
            
            
            
     
        
        
    }else if(_plateType == 2){  //PTA内盘
        
        
        
        if (row == 4 ) {
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdtifierPTAInsideTime];
            
            if (!cell) {
                
                cell = [[[kata_DeliveryTimeCell   alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdtifierPTAInsideTime] autorelease];
                
                
                
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell;
            
        }else{
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdtifierPTAInside];
            
            
            if (!cell) {
                
                cell = [[[kata_SelcetConditionCell   alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdtifierPTAInside] autorelease];
                
                
                
            }
            
            UILabel *nameLab  = (UILabel *)[cell.contentView viewWithTag:11];
            
            nameLab.text = [self.dataArray objectAtIndex:indexPath.row];
            
            if (indexPath.row == ([self.dataArray count] - 1) )
            {
                
                cell.accessoryType   = UITableViewCellAccessoryDisclosureIndicator;
                
            }
            
            UITextField *textFiled = (UITextField *)[cell.contentView viewWithTag:12];
            
            textFiled.text = [self.dataArray objectAtIndex:indexPath.row];
            
            
            if ([textFiled.text isEqualToString:@"方向"])
            {
                textFiled.text = self.dirStr;
            }
            if ([textFiled.text isEqualToString:@"价格"])
            {
                textFiled.text = self.priceAtAalertStr;
            }
            if ([textFiled.text isEqualToString:@"类型"])
            {
                textFiled.text = self.shipStr;
            }
            if ([textFiled.text isEqualToString:@"交割时间"])
            {
                textFiled.text = nil;
            }
            if ([textFiled.text isEqualToString:@"状态"])
            {
                textFiled.text = self.stateStr;
            }
            if ([textFiled.text isEqualToString:@"交割地"])
            {
                
                textFiled.text = self.addressStr;
            }

            
            
            
            
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell;
            
        }

        
        
    }else{ //PTA外盘

     
        
//        if (row == 3 || row == 4) {
//            
//            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdtifierPTAOutTime];
//            
//            if (!cell) {
//                
//                cell = [[[kata_DeliveryTimeCell   alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdtifierPTAOutTime] autorelease];
//                
//                
//                
//            }
//            
//            cell.selectionStyle = UITableViewCellSelectionStyleNone;
//            
//            return cell;
//            
//        }else{
        
            
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdtifierPTAOut];
            
            
            if (!cell) {
                
                cell = [[[kata_SelcetConditionCell   alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdtifierPTAOut] autorelease];
                
                
                
            }
            
            
            
            UILabel *nameLab  = (UILabel *)[cell.contentView viewWithTag:11];
            
            nameLab.text = [self.dataArray objectAtIndex:indexPath.row];
            
            if (indexPath.row == ([self.dataArray count] - 1) )
            {
                
                cell.accessoryType   = UITableViewCellAccessoryDisclosureIndicator;
                
            }
            
            UITextField *textFiled = (UITextField *)[cell.contentView viewWithTag:12];
            
            textFiled.text = [self.dataArray objectAtIndex:indexPath.row];
            
        
        if ([textFiled.text isEqualToString:@"装船时间"])
        {
            textFiled.text = self.dateStr1;
        }
        if ([textFiled.text isEqualToString:@"到港时间"])
        {
            textFiled.text = self.dateStr2;
        }

            if ([textFiled.text isEqualToString:@"方向"])
            {
                textFiled.text = self.dirStr;
            }
            if ([textFiled.text isEqualToString:@"价格"])
            {
                textFiled.text = self.priceAtAalertStr;
            }
            if ([textFiled.text isEqualToString:@"类型"])
            {
                textFiled.text = self.shipStr;
            }
            if ([textFiled.text isEqualToString:@"交割时间"])
            {
                textFiled.text = nil;
            }
            if ([textFiled.text isEqualToString:@"状态"])
            {
                textFiled.text = self.stateStr;
            }
            if ([textFiled.text isEqualToString:@"交割地"])
            {
                
                textFiled.text = self.addressStr;
            }

            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell;
            
            
        }
        
        
//    }

    
    
    
    
    
//   static  NSString *cellIdtifier = @"Cell";
//    
//    static  NSString *cellIdtifierTime = @"CellTime";
    
//    if (indexPath.row != 3) {
//        
//        cell = [[[kata_DeliveryTimeCell  alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdtifierTime] autorelease];
//        
//
//        
//        
//    }else{
//        
//        
//        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdtifier];
//        
//        if (!cell)
//        {
//            
//            
//            cell = [[[kata_SelcetConditionCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdtifier] autorelease];
//            
//            
//        }
//    }
//        
//        
//          
//        
//        
//    }
//  
//    UILabel *nameLab  = (UILabel *)[cell.contentView viewWithTag:11];
//    
//    nameLab.text = [self.dataArray objectAtIndex:indexPath.row];
//    
//    if (indexPath.row == ([self.dataArray count] - 1) )
//    {
//
//        cell.accessoryType   = UITableViewCellAccessoryDisclosureIndicator;
//
//    }
//    
//    UITextField *textFiled = (UITextField *)[cell.contentView viewWithTag:12];
//    
//    textFiled.text = [self.dataArray objectAtIndex:indexPath.row];
//
//   
//    if ([textFiled.text isEqualToString:@"方向"])
//    {
//        textFiled.text = self.dirStr;
//    }
//    if ([textFiled.text isEqualToString:@"价格"])
//    {
//        textFiled.text = self.priceAtAalertStr;
//    }
//    if ([textFiled.text isEqualToString:@"货种"])
//    {
//        textFiled.text = self.goodsStr;
//    }
//    if ([textFiled.text isEqualToString:@"交割时间"])
//    {
//        textFiled.text = nil;
//    }
//    if ([textFiled.text isEqualToString:@"状态"])
//    {
//        textFiled.text = self.stateStr;
//    }
//    if ([textFiled.text isEqualToString:@"交割地"])
//    {
//        
//        textFiled.text = self.addressStr;
//    }
//    
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    return cell;




}

#pragma tableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *str = [self.dataArray objectAtIndex:indexPath.row];
    
    if ([str isEqualToString:@"装船时间"])
    {
        int time1Tag = 1;
        [self pickerShow:time1Tag];
        
    }
    if ([str isEqualToString:@"到港时间"])
    {
        int time2Tag = 2;

        [self pickerShow:time2Tag];
        
    }

    
    
    if ([str isEqualToString:@"方向"])
    {
        
        DebugLog(@"alert");
        
       UIActionSheet *dirSheet = [[UIActionSheet alloc]initWithTitle:@"选择方向" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"卖盘",@"买盘", nil];
        
        dirSheet.tag = 21;
        
        dirSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;

        [dirSheet showInView:self.view];
        
        [dirSheet release];
        
        
    }
    if ([str isEqualToString:@"货种"])
    {
        
        DebugLog(@"alert");
        
        UIActionSheet *dirSheet = [[UIActionSheet alloc]initWithTitle:@"选择货种" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"期货",@"现货", nil];
        
        
        dirSheet.tag = 22;
        
        dirSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
        
        [dirSheet showInView:self.view];
        
        [dirSheet release];
        
        
    }
    if ([str isEqualToString:@"类型"])
    {
        
        DebugLog(@"alert");
        
        UIActionSheet *dirSheet = [[UIActionSheet alloc]initWithTitle:@"选择类型" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"船税",@"保税", nil];
        
        
        dirSheet.tag = 24;
        
        dirSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
        
        [dirSheet showInView:self.view];
        
        [dirSheet release];
        
        
    }

    if ([str isEqualToString:@"状态"])
    {
        
        DebugLog(@"alert");
        
        UIActionSheet *dirSheet = [[UIActionSheet alloc]initWithTitle:@"选择状态" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"成交",@"未成交", nil];
        
        dirSheet.tag = 23;
        
        dirSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
        
        [dirSheet showInView:self.view];
        
        [dirSheet release];
        
        
    }

    
    if ([str isEqualToString:@"交割时间"])
    {
        int delivelyTimeTag = 3;
        [self pickerShow:delivelyTimeTag];
        
        
    }
    
    if ([str isEqualToString:@"交割地"])
    {
        
        kata_AddressViewController *addressVC = [[kata_AddressViewController alloc]init];
        
        addressVC.delegate = self;
        
        [self.navigationController pushViewController:addressVC animated:YES];
        
        [addressVC release];
        
        
    }

    if ([str isEqualToString:@"价格"])
    {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"选择价格" message:@"\n" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        
        
        UITextField *priceText = [[UITextField alloc]initWithFrame:CGRectMake(10, 45, 264, 30)];
        
//        priceText.backgroundColor = [UIColor redColor];
        
        priceText.borderStyle = UITextBorderStyleRoundedRect;
        
//        priceText.text = self.priceAtAalertStr;
        
        priceText.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        
        priceText.contentVerticalAlignment = UIControlContentHorizontalAlignmentCenter;
        
        priceText.delegate = self;
        
        priceText.tag = 1;
        
        [alert addSubview:priceText];
        
        [priceText release];
        
        alert.delegate = self;

        [alert show];
        
        [alert release];
        
    }

    
}

//-(void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
//{
//    
//    
//}

#pragma mark - actionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    DebugLog(@"buttonIndex == %d",buttonIndex);
   
    switch (actionSheet.tag) {
        case 21:  //方向
        {
            if (buttonIndex == 0)
            {
                self.dirStr =  @"卖盘";
                
                [_tableView reloadData];
                
            }
            else
            {
               self.dirStr =  @"买盘";
                
                [_tableView reloadData];

            }
        }
            break;
        case 22:  // 货种
        {
            
            if (buttonIndex == 0)
            {
                self.goodsStr =  @"期货";
                
                [_tableView reloadData];
                
            }
            else
            {
                self.goodsStr =  @"现货";
                
                [_tableView reloadData];
                
            }

        }
            break;
        case 23:  //状态
        {
            if (buttonIndex == 0)
            {
                self.stateStr =  @"成交";
                
                [_tableView reloadData];
                
            }
            else
            {
                self.stateStr =  @"未成交";
                
                [_tableView reloadData];
                
            }

        }
            break;

        case 24:  //类型
        {
            if (buttonIndex == 0)
            {
                self.shipStr =  @"船税";
                
                [_tableView reloadData];
                
            }
            else
            {
                self.shipStr =  @"保税";
                
                [_tableView reloadData];
                
            }
            
        }
            break;

        default:
            break;
    }
    
}
#pragma mark - pickerDelegate
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [UIView beginAnimations:nil context:nil];
    
    [UIView setAnimationDuration:1];
    
    
    _picker.frame = CGRectMake(0, ScreenHeight + 120, ScreenWidth, 120);
    
    
    [UIView commitAnimations];
    

    
}

#pragma mark - alertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    
    
    [_tableView reloadData];

    
    
    DebugLog(@"alertView.frame == %@ ",NSStringFromCGRect(alertView.frame));
}

#pragma mark -  textFieldDelegate

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    DebugLog(@"textField = %@",textField);

}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    DebugLog(@"textField  == %@",textField.text);
    
    
    self.priceAtAalertStr = textField.text;
    
        return YES;

       
    
}

#pragma mark - kata_AddressViewControllerDelegate

- (void)kata_AddressViewControllerAddressName:(NSString *)aAddressName
{
    
    self.addressStr = aAddressName;
    
    [_tableView reloadData];
    
}


#pragma mark - pickerShow
- (void)pickerShow:(int)tag
{
    if (isPicker == NO)
    {
        isPicker = YES;
        
        _picker  = [[UIDatePicker alloc]init];
        
        _picker.frame = CGRectMake(0,ScreenHeight, ScreenWidth, 216);
        
        _picker.datePickerMode = UIDatePickerModeDate;
        
        _picker.tag = tag;
        
        [_picker addTarget:self action:@selector(changeTime:) forControlEvents:UIControlEventValueChanged];
        
        [UIView beginAnimations:nil context:nil];
        
        [UIView setAnimationDuration:1];
        
        //        [UIView setAnimationDelegate:self];
        //
        //        [UIView setAnimationDidStopSelector:@selector(animationDidStop:
        //                                                      finished:
        //                                                      context:)];
        
        _picker.frame = CGRectMake(0, ScreenHeight - 216 - 49, ScreenWidth, 216);
        
        
        
        [UIView commitAnimations];
        
        [self.view addSubview:_picker];
        
    }

}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
