//
//  kata_AddressViewController.m
//  LargeClan
//
//  Created by kata on 13-11-27.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import "kata_AddressViewController.h"
#import "KATAUtils.h"
#import "ASIFormDataRequest.h"
#import "KATAConstants.h"

#import "NSString+MD5.h"
#import "KATAAddress.h"
@interface kata_AddressViewController ()

{
    UITableView *_tableView;
}
@property(nonatomic,retain)NSDictionary *getDic;

@property(nonatomic,retain)NSMutableArray *getArr;

@end

@implementation kata_AddressViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

        
    }
    return self;
}

- (void)dealloc {
    
    [self.getArr release];
    
    [_tableView release];
    
    [super dealloc ];
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    
    [self.navigationItem setHidesBackButton:YES];
    
    
    
    
    NSString *urlStr = [NSString stringWithFormat:@"%@",SERVER_URI];

    ASIFormDataRequest *request = [[ASIFormDataRequest alloc]initWithURL:[NSURL URLWithString:urlStr]];
    
    [request setPostValue:[self sign] forKey:@"sign"];
    
    [request setPostValue:[self method] forKey:@"method"];
    
    [request setPostValue:[self appkey] forKey:@"appkey"];
    
    [request setPostValue:[self params] forKey:@"params"];
    
    [request setPostValue:[NSString stringWithFormat:@"%ld",[self time]] forKey:@"time"];

    
    request.timeOutSeconds = 20;
    
    request.delegate  = self;
    
    [request startAsynchronous];
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self beginingView];
   
    
    self.getArr  = [[NSMutableArray alloc]init];
    
   _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 20, ScreenWidth, ScreenHeight) style:UITableViewStyleGrouped];
    
    _tableView.backgroundView = nil;
    
    
    _tableView.delegate = self;
    
    _tableView.dataSource = self;
    
    [self.view addSubview:_tableView];

    

}

- (NSString *)sign
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    NSMutableString *sign = [NSMutableString stringWithString:@"10010 +"];
    
    [sign appendFormat:@"%@ + %ld",[self method],timestamp];
    
    [sign MD5];
    
    return [sign description] ;
}


- (NSString *)appkey
{
    return @"10010";
    
}
- (NSString *)method
{
    return @"index_placemessage_get";
}

- (NSDictionary *)params
{
    int i = 1;
    
    NSString *nameofpart = [NSString stringWithFormat:@"%d",i];
    
   _getDic = [[[NSDictionary alloc]initWithObjectsAndKeys:
              nameofpart,@"nameofpart",
              nil] autorelease];
    
    
    return _getDic;
    
    
}

- (long int )time
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    return timestamp;
}

#pragma mark - custom
-(void)beginingView
{
    self.title = @"交割地";
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    backBtn.frame = CGRectMake(0, 0, 44, 44);
    
    [backBtn setImage:[UIImage imageNamed:@"return.png"] forState:UIControlStateNormal];
    
    [backBtn addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    self.navigationItem.leftBarButtonItem  = leftItem;
    
    [leftItem release];
    
}



-(void)backClick
{
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

#pragma tableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return 6;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static  NSString *cellIdtifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdtifier];
    
    if (!cell)
    {
        
        cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdtifier] autorelease];
        
        
        
        
    }
    
    if ([self.getArr count] != 0) {
        
        KATAAddress  *address= [self.getArr objectAtIndex:indexPath.row];
        
        cell.textLabel.text = address.addressName;
    }
    
  

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma tableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    KATAAddress  *address= [self.getArr objectAtIndex:indexPath.row];

    
    if ([self.delegate respondsToSelector:@selector(kata_AddressViewControllerAddressName:)]) {
        
        
        [self.delegate kata_AddressViewControllerAddressName:address.addressName];
        
    }
    
    
    [self.navigationController popViewControllerAnimated:YES];
    
}



#pragma mark - ASIHttpRequestDelegate

- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:[request responseData] options:NSJSONReadingMutableLeaves error:nil];
    
    
    if ([[dic objectForKey:@"code"] isEqualToString:@"0"])  {
        
        
        NSDictionary *dataDic = [dic objectForKey:@"data"];
        
        NSArray  *brands = [dataDic objectForKey:@"brands"];
        
        NSString *code = [dataDic objectForKey:@"code"];
        
        NSArray *goodsSources= [dataDic objectForKey:@"goodsSources"];
        
        
        for (int i = 0; i < [goodsSources count]; i++) {
            
            
            
            KATAAddress *address  = [[KATAAddress alloc]init];
            
            
            NSDictionary *dic = [goodsSources objectAtIndex:i];
            
            NSString *goodsSourcename = [dic   objectForKey:@"goodsSourcename"];
            
            
            address.addressName = goodsSourcename;
            
            NSString *addressId = [dic objectForKey:@"id"];
            
            
            [self.getArr addObject:address];
            
            [address release];
            
            
        }
        
        
        [_tableView reloadData];

        
    }
    
    
    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
