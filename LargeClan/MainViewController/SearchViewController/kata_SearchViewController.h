//
//  kata_SearchViewController.h
//  LargeClan
//
//  Created by kata on 13-11-19.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "kata_segView.h"
#import "kata_AddressViewController.h"
@interface kata_SearchViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,kata_segViewDelegate,UIActionSheetDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIAlertViewDelegate,UITextFieldDelegate,
    kata_AddressViewControllerDelegate>


@end
