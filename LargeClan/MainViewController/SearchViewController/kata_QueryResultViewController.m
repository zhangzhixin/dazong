//
//  kata_QueryResultViewController.m
//  LargeClan
//
//  Created by kata on 13-12-28.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import "kata_QueryResultViewController.h"

#import "kata_TwoTableView.h"
#import "KATAUtils.h"

#import "ASIFormDataRequest.h"

#import "KATAConstants.h"

#import "NSString+MD5.h"

@interface kata_QueryResultViewController ()

{
    int _type;
}
@property(nonatomic,retain)NSDictionary *postDic;

@end


@implementation kata_QueryResultViewController

- (id)initWithType:(int)aType
{
    if (self = [super init]) {
        
        
        _type = aType;
        
        
    }
    return self;
}

- (void)viewDidLoad
{
    
    self.navigationItem.hidesBackButton = YES;
    
    
//    kata_TwoTableView *twoTableView = [[kata_TwoTableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight) ];
    
    kata_TwoTableView *twoTableView = [[kata_TwoTableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    
    
    [self.view addSubview:twoTableView];
    
    [twoTableView release];
    
    
}


- (void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    
    

    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:SERVER_URI]];
    
    
    
    
    
    request.timeOutSeconds = 20;

    request.delegate = self;
    
    
    [request startAsynchronous];
    
    
    
    
    
    
}
#pragma mark - ASIFormDataRequestDelegate

- (void)requestFinished:(ASIHTTPRequest *)request
{
    

    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    
    
}


#pragma mark - sign

- (NSString *)sign
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    NSMutableString *sign = [NSMutableString stringWithString:@"10010 +"];
    
    [sign appendFormat:@"%@ + %ld",[self method],timestamp];
    
    [sign MD5];
    
    return [sign description] ;
}


- (NSString *)appkey
{
    return @"10010";
    
}
- (NSString *)method
{
    return @"index_offer_get";
}

- (NSDictionary *)params
{
    
    
    NSString *nameofpart = [NSString stringWithFormat:@"%d",_type];
    
    
    _postDic = [[[NSMutableDictionary alloc]initWithObjectsAndKeys:
                 nameofpart,@"nameofpart",
                 
                 nil] autorelease];
    
    
    return _postDic;
    
}

- (long int )time
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    return timestamp;
}











@end
