//
//  kata_QueryResultViewController.h
//  LargeClan
//
//  Created by kata on 13-12-28.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"

@interface kata_QueryResultViewController : UIViewController <ASIHTTPRequestDelegate>

@end
