//
//  kata_segView.m
//  LargeClan
//
//  Created by kata on 13-11-25.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import "kata_segView.h"
#import "KATAUtils.h"

@interface kata_segView ()
{
    UIButton *_button;
}
@property(nonatomic,retain)NSMutableArray *insideArrayMEG;
@property(nonatomic,retain)NSMutableArray *outerArrayMEG;
@property(nonatomic,retain)NSMutableArray *insideArrayPTA;
@property(nonatomic,retain)NSMutableArray *outerArrayPTA;

@property(nonatomic,retain)NSMutableArray *insideIndexArrayMEG;
@property(nonatomic,retain)NSMutableArray *outerIndexArrayMEG;
@property(nonatomic,retain)NSMutableArray *insideIndexArrayPTA;
@property(nonatomic,retain)NSMutableArray *outerIndexArrayPTA;


@end


@implementation kata_segView


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        
        
//        UISegmentedControl *seg = [[UISegmentedControl alloc]initWithItems:array];
//        
//        
//        seg.frame = CGRectMake(0, 0, 320, 40);
//        
//        [seg setBackgroundImage:[UIImage imageNamed:@"nav.png"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
//        
//        [seg setBackgroundImage:[UIImage imageNamed:@"nav_active.png"] forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
//        
//        seg.backgroundColor = [UIColor clearColor];
        

//        [seg setDividerImage:[UIImage imageNamed:@"nav.png"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        
//        [seg setDividerImage:[UIImage imageNamed:@"nav.png"] forLeftSegmentState:UIControlStateSelected rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
//        
//        [seg setDividerImage:[UIImage imageNamed:@"nav_active.png"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
        
        
//        self.userInteractionEnabled = YES;
//
//        [self addSubview:seg];
//
        self.insideArrayMEG = [[NSMutableArray alloc]initWithObjects:@"方向",@"价格",@"货种",@"交割时间",@"状态",@"交割地", nil];
        self.outerArrayMEG = [[NSMutableArray alloc]initWithObjects:@"方向",@"价格",@"类型",@"装船时间",@"到港时间",@"状态",@"交割地", nil];

        self.insideArrayPTA = [[NSMutableArray alloc]initWithObjects:@"方向",@"价格",@"配送",@"货种",@"交割时间",@"状态",@"交割地", nil];

        self.outerArrayPTA = [[NSMutableArray alloc]initWithObjects:@"方向",@"价格",@"类型",@"装船时间",@"到港时间",@"状态",@"交割地", nil];

        
        
        self.insideIndexArrayMEG = [[NSMutableArray alloc]initWithObjects:@"方向",@"人民币",@"货种",@"交割时间",@"交割地",@"保证金",@"数量（吨）",@"报盘人",@"发票",@"免仓",@"状态",@"备注", nil];
        self.outerIndexArrayMEG = [[NSMutableArray alloc]initWithObjects:@"方向",@"美金（CFR）",@"类型",@"货源地",@"预装船",@"预到港",@"转手",@"交单",@"交割地",@"数量（吨）",@"报盘人",@"清单可借",@"付款方式",@"状态",@"备注", nil];
        
        self.insideIndexArrayPTA = [[NSMutableArray alloc]initWithObjects:@"方向",@"人民币",@"配送",@"品牌",@"货种",@"交割时间",@"交割地",@"数量（吨）",@"报盘人",@"保证金",@"发票",@"状态",@"备注", nil];
        
        self.outerIndexArrayPTA = [[NSMutableArray alloc]initWithObjects:@"方向",@"美金（CFR）",@"类型",@"货源地",@"品牌",@"预装船",@"预到港",@"转手",@"交单",@"交割地",@"数量（吨）",@"报盘人",@"清单可借",@"付款方式",@"状态",@"备注", nil];


        
        NSArray *array = [[NSArray alloc]initWithObjects:@"MEG内盘",@"MEG外盘",@"PTA内盘",@"PTA外盘", nil];
        
        [array autorelease];

        for (int i = 0; i < 4; i ++)
        {
            _button = [UIButton buttonWithType:UIButtonTypeCustom];
            
            _button.frame = CGRectMake(i * 80, 0, 80, 40);
            
            [_button.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
            
            _button.tag = i + 1;
            
            if (_button.tag == 1)
            {
                [_button setBackgroundImage:[UIImage imageNamed:@"nav_active.png"] forState:UIControlStateNormal];

            }
            else
            {
                [_button setBackgroundImage:[UIImage imageNamed:@"nav.png"] forState:UIControlStateNormal];

            }
            
            [_button setTitle:[array objectAtIndex:i] forState:UIControlStateNormal];
            
            
            [_button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            

        
            [_button addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
            
            
            [self addSubview:_button];
            
        }
        
        
        
    }
    return self;
}


-(void)dealloc
{
    
    [self.insideArrayPTA release];
    [self.outerArrayPTA release];
    [self.insideArrayMEG release];
    [self.outerArrayMEG release];
    
    [self.insideIndexArrayPTA release];
    [self.outerIndexArrayPTA release];
    [self.insideIndexArrayMEG release];
    [self.outerIndexArrayMEG release];
    
    [super dealloc];
}

#pragma mark - buttonClick

-(void)btnClick:(id)sender
{
    DebugLog(@"seg 点击");

    UIButton *btn = (UIButton *)sender;
    
    // 互斥btn
    for (UIView *subView in self.subviews)
    {
        
        if ([subView isKindOfClass: [UIButton class]])
        {
            
            UIButton *btnView = (UIButton *)subView;
            
            if (btnView.tag == btn.tag)
            {
                [btn setBackgroundImage:[UIImage imageNamed:@"nav_active.png"] forState:UIControlStateNormal];
            }
            else
            {
                [btnView setBackgroundImage:[UIImage imageNamed:@"nav.png"] forState:UIControlStateNormal];
            }
            
            
        }
    }
    
    //条条
    if ([self.delegate respondsToSelector:@selector(changeNameData:)]) {
        
        
        switch (btn.tag)
        {
            case 1:
            {
                [self.delegate changeNameData:self.insideArrayMEG];
            }
                break;
            case 2:
            {
                [self.delegate changeNameData:self.outerArrayMEG];

            }
                break;
            case 3:
            {
                [self.delegate changeNameData:self.insideArrayPTA];

            }
                break;
            case 4:
            {
                [self.delegate changeNameData:self.outerArrayPTA];

            }
                break;
                
            default:
                break;
        }
        
        
        
    }
    
   
    
    if ([self.delegate respondsToSelector:@selector(changeIndexNameData:)]) {
        
        
            switch (btn.tag)
            {
                case 1:
                {
                    [self.delegate changeIndexNameData:self.insideIndexArrayMEG];
                }
                    break;
                case 2:
                {
                    [self.delegate changeIndexNameData:self.outerIndexArrayMEG];
                    
                }
                    break;
                case 3:
                {
                    [self.delegate changeIndexNameData:self.insideIndexArrayPTA];
                    
                }
                    break;
                case 4:
                {
                    [self.delegate changeIndexNameData:self.outerIndexArrayPTA];
                    
                }
                    break;
                    
                default:
                    break;
            }
        
        }

    
    if ([self.delegate respondsToSelector:@selector(changeIndex:)]) {
        
        
        [self.delegate changeIndex:btn.tag];
        
    }
    

    
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
