//
//  kata_segView.h
//  LargeClan
//
//  Created by kata on 13-11-25.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol kata_segViewDelegate <NSObject>

-(void)changeNameData:(NSMutableArray *)array;

-(void)changeIndexNameData:(NSArray *)array;

-(void)changeIndex:(int)index;


@end

@interface kata_segView : UIView
{
    id <kata_segViewDelegate> delegate;
}
@property(nonatomic,assign)    id <kata_segViewDelegate> delegate;

@end
