//
//  kata_TwoTableView.m
//  LargeClan
//
//  Created by kata on 13-12-28.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import "kata_TwoTableView.h"
#import "KATAUtils.h"

#import "kata_LeftTableDelegate.h"

#import "kata_RightTableDelegate.h"

@interface kata_TwoTableView ()
{
    int _type;
}

@end

@implementation kata_TwoTableView

- (id)initWithType:(int)aType
{
    if (self  = [super init]) {
        
        _type = aType;
    }
    return self;
}


- (id)initWithFrame:(CGRect)frame
{
    
    if (self = [super initWithFrame:frame ]) {
        
        
      _leftDelegate  = [[kata_LeftTableDelegate alloc]init];
        
        _rightDelegate = [[kata_RightTableDelegate alloc]init];
        
        
        
        UITableView *leftTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, 140, ScreenHeight) style:UITableViewStylePlain];
        
        
        leftTableView.backgroundColor = [UIColor redColor];
        
        leftTableView.delegate = _leftDelegate;
        
        leftTableView.dataSource = _leftDelegate;
        
        
        [self addSubview:leftTableView];
        
        [leftTableView release];
        
//        [_leftDelegate release];
        
       UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(140, 0, 220, ScreenHeight)];
        
//        scrollView.contentSize = CGSizeMake([_firstRowArrayInTableView2 count] * LABELWIDTH, 0);
        
         scrollView.contentSize = CGSizeMake(1200, 0);

        
        [self addSubview:scrollView];
        
        
        
//       UITableView *rightTableView  = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, [_firstRowArrayInTableView2 count] * LABELWIDTH, ScreenHeight) style:UITableViewStylePlain];
        
        
        UITableView *rightTableView  = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, 1200, ScreenHeight) style:UITableViewStylePlain];
        
        
        rightTableView.delegate = _rightDelegate;
        
        rightTableView.dataSource = _rightDelegate;
        
        rightTableView.backgroundColor = [UIColor greenColor];
        
    
        [scrollView addSubview:rightTableView];
        
        [rightTableView release];
        
//        [_rightDelegate release];

        
        
    }
    return self;
    
}
- (void)dealloc
{
    [_leftDelegate release];
    
    [_rightDelegate release];
    
    [super dealloc];
}

//#pragma mark - leftTableViewDelegate
//
//
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    
//    
//    return 2;
//}
//
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    
//    static  NSString *CellIdentifier = @"Cell";
//    
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    
//    if (!cell) {
//        
//        cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
//        
//        
//        
//    }
//    
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    
//    return cell;
//    
//    
//    
//}








@end
