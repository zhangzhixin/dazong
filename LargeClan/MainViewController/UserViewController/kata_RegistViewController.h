//
//  kata_RegistViewController.h
//  LargeClan
//
//  Created by kata on 13-12-12.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
#import <MessageUI/MessageUI.h>

@protocol kata_RegistViewControllerDelegate <NSObject>

- (void)kata_RegistViewControllerHandPhone:(NSString *)aHandPhone;

@end

@interface kata_RegistViewController : UITableViewController <     UITextFieldDelegate,
    UIActionSheetDelegate,
    ASIHTTPRequestDelegate
    >

{
    id <kata_RegistViewControllerDelegate> delegate;
    
}

@property (nonatomic,assign) id <kata_RegistViewControllerDelegate> delegate;

@end
