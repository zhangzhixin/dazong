//
//  kata_UserViewController.m
//  LargeClan
//
//  Created by kata on 13-11-25.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import "kata_UserViewController.h"
#import "KATAUtils.h"
//#import "kata_registViewController.h"
#import "KATAUITabBarController.h"
#import "kata_Tools.h"

#import "kata_RegistViewController.h"

#import "KATAConstants.h"
#import "ASIFormDataRequest.h"
#import "NSString+MD5.h"
#import "kata_UserInfoViewController.h"

@interface kata_UserCell:UITableViewCell

@end
@implementation kata_UserCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
	{

        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 2, 40, 40)];

        imageView.tag = 11;
        
//        imageView.backgroundColor = [UIColor blueColor];
        
        [self.contentView addSubview:imageView];
        
        [imageView release];
        
		UITextField *textField = [[UITextField alloc]initWithFrame:CGRectMake(50, 7, 220, 30)];
        
        textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        
//        textField.backgroundColor =[UIColor redColor];
        
        textField.tag = 12;
        
        [self.contentView addSubview:textField];

        
        [textField release];
        
		
				
    }
    return self;
}


@end


@interface kata_UserViewController ()

{
    UITextField *_nameTextField;
    
    UITextField *_passwordTextField;
    
    UITableView *_tableView;
}

@property(nonatomic,retain)NSMutableDictionary *params;

@property(nonatomic,retain)NSString *enterName;
@property(nonatomic,retain)NSString *enterPassword;


@property(nonatomic,retain)NSArray *nameArray;
@property(nonatomic,retain)NSArray *imageArray;


@end

@implementation kata_UserViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        self.hidesBottomBarWhenPushed = YES;
        
       UIImage* img1 =[UIImage imageNamed:@"header_but.png"];
//        UIImage* img2 =[UIImage imageNamed:@"header_but.png"];
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        btn.frame =CGRectMake(0, 0, 44, 32);
        
        [btn setTitle:@"注册" forState:UIControlStateNormal];

        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

     [btn setBackgroundImage:img1 forState:UIControlStateNormal];
     
//        [btn setBackgroundImage:img2 forState:UIControlStateHighlighted];
        
        
        [btn addTarget: self action: @selector(registButtonClick) forControlEvents: UIControlEventTouchUpInside];
        
        UIBarButtonItem* item=[[UIBarButtonItem alloc]initWithCustomView:btn];
        
        self.navigationItem.rightBarButtonItem = item;
        
        [item release];


    }
    return self;
}

- (void)dealloc
{
    
    [_tableView release];
    
    [super dealloc];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self beginingView];
    
/////////////////////////////////////////////////////////////////////////////

    _tableView    = [[UITableView alloc]initWithFrame:CGRectMake(0, 20, ScreenWidth, ScreenHeight) style:UITableViewStyleGrouped];
    
    _tableView.backgroundView = nil;
    
    
    _tableView.delegate = self;
    
    _tableView.dataSource = self;
    
    [self.view addSubview:_tableView];
    
    
    self.nameArray = [[[NSArray alloc]initWithObjects:@"输入账号",@"输入密码",nil] autorelease];
    
    
    self.imageArray = [[[NSArray alloc]initWithObjects:@"username.png",@"password.png",nil] autorelease];
    
    
///////////////////////////////////////////////////////////////////////////////
        
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    
    
    KATAUITabBarController *tabBarController = (KATAUITabBarController *)self.tabBarController;
    
    [tabBarController showTabBar];
    
    
    
}

#pragma mark - beginingView

-(void)beginingView
{
    self.title = @"登录";
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"header.png"] forBarMetrics:UIBarMetricsDefault];
    
    
}

#pragma mark - regist && enter

-(void)registButtonClick
{
    
    DebugLog(@"注册");
    
    kata_RegistViewController *registVC =[[kata_RegistViewController alloc]init];
    
    registVC.delegate = self;
    
    [self.navigationController   pushViewController:registVC animated:YES];
    
    [registVC release];
    
    
}
-(void)confirmButtonClick
{
    
    DebugLog(@"登录");
    
    if ([_nameTextField.text length] ==  0 || [_passwordTextField.text length] == 0)
    {
        
        [kata_Tools myAlertView:@"登录提示:" message:@"账号密码不能为空" cancelButtonTitle:@"确定" otherButtonTitles:nil];
    }
    
    NSString *urlStr  = [NSString stringWithFormat:@"%@",SERVER_URI];
    
    
    ASIFormDataRequest    *request  = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlStr]];
    
    [request setPostValue:[self sign] forKey:@"sign"];
    
    [request setPostValue:[self method] forKey:@"method"];
    
    [request setPostValue:[self appkey] forKey:@"appkey"];
    
    [request setPostValue:[self params] forKey:@"params"];
    
    [request setPostValue:[NSString stringWithFormat:@"%ld",[self time]] forKey:@"time"];
    
    request.timeOutSeconds  = 20;
    
    request.delegate = self;
    
    [request startAsynchronous];
    
}

#pragma mark - tableViewDataSoure

-(float)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 154.0;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view  = [[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 154)] autorelease];
    
//    view.backgroundColor = [UIColor redColor];
    
    UIButton *checkButton = [UIButton buttonWithType:UIButtonTypeCustom];

    checkButton.frame = CGRectMake(20, 20, 21, 21);


    [checkButton setImage:[UIImage imageNamed:@"selected.png"] forState:UIControlStateNormal];

    [checkButton addTarget:self action:@selector(chageSelect) forControlEvents:UIControlEventTouchUpInside];

    [view addSubview:checkButton];
    

    UILabel *lab = [[UILabel alloc]initWithFrame:CGRectMake(45, 20, 80, 21)];
    
    lab.backgroundColor = [UIColor clearColor];
    
    lab.text = @"记住密码";
    
    [view addSubview:lab];
    
    [lab release];
    
    
    UIButton *foundButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    foundButton.frame = CGRectMake(185, 20, 80, 21);
    
    [foundButton setTitle:@"找回密码" forState:UIControlStateNormal];
    
    [foundButton addTarget:self action:@selector(foundPassword) forControlEvents:UIControlEventTouchUpInside];
    
    [foundButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    
    [view addSubview:foundButton];
    
    
    UIButton *confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    confirmButton.frame = CGRectMake(14, 71, 292, 43);
    
    [confirmButton setBackgroundImage:[UIImage imageNamed:@"regbut.png"] forState:UIControlStateNormal];
    
    
    [confirmButton setTitle:@"登录" forState:UIControlStateNormal];
    
    [confirmButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    
    [confirmButton addTarget:self action:@selector(confirmButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    [view addSubview:confirmButton];
    
    return view;

    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"cellIdentifer";
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    if (!cell)
    {
        cell = [[[kata_UserCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
        
    }
    
    UIImageView *imageView = (UIImageView *)[cell.contentView viewWithTag:11];
    
    [imageView setImage:[UIImage imageNamed:[self.imageArray objectAtIndex:indexPath.row]]];
    
    if (indexPath.row == 0)
    {
        
        _nameTextField = (UITextField *)[cell.contentView viewWithTag:12];
        
        _nameTextField.placeholder = [self.nameArray objectAtIndex:indexPath.row];
        
        _nameTextField.delegate = self;
        
        self.enterName = _nameTextField.text;
    }
    else //密码
    {
       _passwordTextField  = (UITextField *)[cell.contentView viewWithTag:12];
        
        _passwordTextField.placeholder = [self.nameArray objectAtIndex:indexPath.row];
        
        _passwordTextField.delegate = self;
        
        self.enterPassword = _passwordTextField.text;

        
    }
    
    
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
    
}
#pragma mark - buttonEvent

-(void)chageSelect
{
    DebugLog(@"记住密码");
    
    
    
}



- (NSString *)sign
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    NSMutableString *sign = [NSMutableString stringWithString:@"10010 +"];
    
    [sign appendFormat:@"%@ + %ld",[self method],timestamp];
    
    [sign MD5];
    
    return [sign description] ;
}


- (NSString *)appkey
{
    return @"10010";
    
}
- (NSString *)method
{
    return @"user_login_post";
}

- (NSDictionary *)params
{
    
    
    DebugLog(@" self.enterName ==  %@, enterPassword == %@", self.enterName,self.enterPassword);
    
    _params = [[[NSMutableDictionary alloc]initWithObjectsAndKeys:
                    
                    self.enterName,@"handphone",
                    
                    [self.enterPassword MD5],@"md5pass",
                    
                    nil] autorelease];

    return _params;
    
}

- (long int )time
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    return timestamp;
}



//#pragma mark -alertDelegate
//- (void)willPresentAlertView:(UIAlertView *)alertView
//
//{
//    
//    DebugLog(@"alertView == %@",NSStringFromCGRect(alertView.frame));
//    
//}

#pragma mark - custom
-(void)foundPassword
{
    
    DebugLog(@"找回密码");
    
    
}



#pragma mark - ASIHttpDelegate

- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    DebugLog(@"request == %@",[request responseString]);
    
    
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:[request responseData] options:NSJSONReadingMutableLeaves error:nil];
    
    
    
    NSString *status = [dic objectForKey:@"status"];
    
    
    if ([status isEqualToString:@"OK"]) {  //登陆成功
        
        
        
//        NSString *code = [dic objectForKey:@"code"];
//        
//        NSDictionary *dataDic = [dic objectForKey:@"data"];
//        
//        NSString *dataCode = [dataDic objectForKey:@"code"];
//        NSString *token = [dataDic objectForKey:@"token"];
//        NSString *uid = [dataDic objectForKey:@"uid"];
//        
//        
//        
//        NSString *msg = [dic objectForKey:@"msg"];
//        
//        
//        [self.navigationController popViewControllerAnimated:YES];
        
        
        
        kata_UserInfoViewController *userInfoVC = [[kata_UserInfoViewController alloc]init];

        
    
        [self.navigationController pushViewController:userInfoVC animated:YES];
        
        [userInfoVC release];
        
    }else
    {
        
        [kata_Tools myAlertView:@"温馨提示:" message:@"登陆错误" cancelButtonTitle:@"确定" otherButtonTitles:nil];
        
    }
    
    
    
    
    
    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    DebugLog(@"requestFailedrequestFailed");
    
}

#pragma mark - textFiledDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField

{
    if (textField.tag == 12) {
        
        
//        self.enterPassword = textField.text;
    }
    if (textField.tag == 11) {
        
//        self.enterName = textField.text;
    }
    
    
    [_passwordTextField resignFirstResponder];
    
    [_nameTextField resignFirstResponder];
    
    return YES;
    
}

#pragma mark -  kata_RegistViewControllerHandPhone

- (void)kata_RegistViewControllerHandPhone:(NSString *)aHandPhone
{
    
    self.enterName = aHandPhone;
    
    [_tableView reloadData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
