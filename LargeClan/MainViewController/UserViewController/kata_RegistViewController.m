//
//  kata_RegistViewController.m
//  LargeClan
//
//  Created by kata on 13-12-12.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import "NSString+Addtions.h"

#import "KATAUtils.h"

#import "kata_Tools.h"

#import "ASIHTTPRequest.h"

#import "ASIFormDataRequest.h"

#import "KATAConstants.h"

#import "kata_VerificationRequest.h"

#import "NSString+MD5.h"

#import <MessageUI/MessageUI.h>

@interface kata_UserRegistCell : UITableViewCell <UITextFieldDelegate,ASIHTTPRequestDelegate,
    MFMessageComposeViewControllerDelegate>

{
    UITextField *_handPhoneTextField;
}

@property (nonatomic,retain) NSMutableDictionary *postDic;

@property (nonatomic,retain) NSString *msgStr;

@property (nonatomic,retain) NSString *handPhone;
@end

@implementation kata_UserRegistCell

- (void)dealloc
{
    [_handPhoneTextField release];

    [super dealloc];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 300, 40 + 20 + 30 + 44)];
        
        view.tag = 10;
        
        UILabel *lab = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 300, 20)];
        
        lab.tag  = 1;
        
        lab.text  =  @"手机号码";
        
        [lab setFont:[UIFont systemFontOfSize:16.0]];
        
        lab.backgroundColor = [UIColor clearColor];
        
        [view addSubview:lab];
        
        [lab release];
        
        _handPhoneTextField  = [[UITextField alloc]initWithFrame:CGRectMake(10, lab.frame.origin.y  + 20 + 10, 300, 30)];
        
        _handPhoneTextField.delegate = self;
        
        _handPhoneTextField.borderStyle  = UITextBorderStyleBezel;
        
        _handPhoneTextField.tag  = 2;
        
        [view addSubview:_handPhoneTextField];
        
        
        
        
        UIButton *button  = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        
        button.frame  =  CGRectMake(10, _handPhoneTextField.frame.origin.y + 30 + 10, 300, 44);
        
        [button setTitle:@"免费获取验证码" forState:UIControlStateNormal];
        
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [button addTarget:self action:@selector(VerificationBtnClick) forControlEvents:UIControlEventTouchUpInside];
        
        
        button.tag  = 3;
        
        [view addSubview:button];
        
        
       [self.contentView addSubview:view];
        
        [view    release];
        
        
    }
 
    return self;
}




- (NSString *)sign
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    NSMutableString *sign = [NSMutableString stringWithString:@"1001 +"];
    
    [sign appendFormat:@"%@ + %ld",[self method],timestamp];
    
    [sign MD5];
    
    return [sign description] ;
}


- (NSString *)appkey
{
    return @"10010";
    
}
- (NSString *)method
{
    return @"user_sendVerification_update";
}

- (NSMutableDictionary *)params
{
    NSString *info = @"1";
      
    self.postDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:info,@"info",self.handPhone,@"handphone", nil];
    
    
    return self.postDic;
    
}

- (long int )time
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];

    return timestamp;
}

- (void)VerificationBtnClick  // 验证码按钮
{
        
    
    
    NSString *urlStr  = [NSString stringWithFormat:@"%@",SERVER_URI];
    
        
    ASIFormDataRequest    *request  = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlStr]];
    
    [request setPostValue:[self sign] forKey:@"sign"];
    
    [request setPostValue:[self method] forKey:@"method"];
    
    [request setPostValue:[self appkey] forKey:@"appkey"];
    
    [request setPostValue:[self params] forKey:@"params"];
    
    [request setPostValue:[NSString stringWithFormat:@"%ld",[self time]] forKey:@"time"];
    
        request.timeOutSeconds  = 20;
        
        request.delegate = self;
        
        [request startAsynchronous];
    
    if ([MFMessageComposeViewController canSendText]) {
        [self showSendMessage];
    }
    else {
        [self alertWithTitle:@"温馨提示" msg:@"当前短信功能不可用"];
    }

    
}

//发短信
-(void)showSendMessage
{
    MFMessageComposeViewController *message = [[MFMessageComposeViewController alloc] init];
    message.recipients = [NSArray arrayWithObjects:self.handPhone, nil];
    message.body = self.msgStr;
    message.messageComposeDelegate = self;
    
	//[self presentModalViewController:message animated:YES];
    
	[self presentViewController:message animated:YES completion:nil];
    
    [message release];
}

#pragma mark -wxg
#pragma mark 提示
- (void) alertWithTitle: (NSString *)title_ msg: (NSString *)msg
{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title_ message:msg delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
    [alert show];
    [alert release];
    
}

#pragma mark messageDelegate

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    
    NSString *title = @"Mail";
    NSString *msg = nil;
    
    switch (result) {
        case MessageComposeResultCancelled:
            msg = @"短信发送取消";
            break;
        case MessageComposeResultSent:
            msg = @"短信发送成功";
            [self alertWithTitle:title msg:msg];
            break;
        case MessageComposeResultFailed:
            msg = @"短信发送失败";
            [self alertWithTitle:title msg:msg];
            break;
        default:
            msg = @"短信没有发送";
            [self alertWithTitle:title msg:msg];
            break;
    }
	
	//[controller dismissModalViewControllerAnimated:YES];
    [controller dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark  -  asihttpDelegate

- (void)requestFinished:(ASIFormDataRequest *)request
{
    
    DebugLog(@"request ==  %@",[request responseString]);
    
    
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:[request responseData] options:NSJSONReadingMutableLeaves error:nil];
    
    if ([[dic objectForKey:@"status"] isEqualToString:@"ok"]) {  //成功
        
        
        NSString *code  = [dic objectForKey:@"code"];
        NSString *msg  = [dic objectForKey:@"msg"];
        
        NSDictionary *data = [dic objectForKey:@"data"];
        NSString *dataCode  = [data objectForKey:@"code"];
        NSString *token  = [data objectForKey:@"token"];
        NSString *uid  = [data objectForKey:@"uid"];

        self.msgStr = msg;
        
        
        
        
    }
    
    
    
    
}

- (void)requestFailed:(ASIFormDataRequest *)request
{
    
    DebugLog(@"requestFailedrequestFailed");

    
}

#pragma mark - textFiledDelegate


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    self.handPhone = textField.text;
    
    
    return YES;
    
}


-  (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_handPhoneTextField resignFirstResponder];
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    DebugLog(@"string == %@",string);
    
    if (range.location >= 11) {
        
        return NO;
        
    }else{
        
        return YES;

    }
    
    
    
}

@end





////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#import "kata_RegistViewController.h"
#import "kata_registCell.h"
#import "KATAUtils.h"
#import "KATAUITabBarController.h"
#import "KATARegInfo.h"

@interface kata_RegistViewController ()

{
    
    KATARegInfo *_regInfo;
    
}

@property (nonatomic,retain) NSMutableDictionary *postRegDic;

@property(nonatomic,retain)NSArray *cellName;

@property(nonatomic,retain)NSString *companyTypeStr;


@property(nonatomic,retain)NSString *handPhone;



@end

@implementation kata_RegistViewController

- (void)dealloc
{
    
    [_regInfo release];
    
    [self.cellName release];
    
    [super dealloc];
}
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    
//        self.storyboard
    
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    [self beginingView];
    
    

    
  self.cellName = [NSArray arrayWithObjects:@"验证码",@"登录密码",@"确认密码",@"公司类型",@"公司名称",@"公司简称",@"公司电话",@"实际操盘人",@"QQ", nil];
    
   _regInfo = [[KATARegInfo alloc]init];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    KATAUITabBarController *tab  = (KATAUITabBarController  *)self.tabBarController;
    
    [tab hideTabBar];

    
}

#pragma mark - custom
-(void)beginingView
{
    self.title = @"用户注册";
    
    self.companyTypeStr = @"选择公司的类型";
    
    self.navigationItem.hidesBackButton = YES;

    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    backBtn.frame = CGRectMake(0, 0, 44, 44);
    
    [backBtn setImage:[UIImage imageNamed:@"return.png"] forState:UIControlStateNormal];
    
    [backBtn addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    self.navigationItem.leftBarButtonItem  = leftItem;
    
    [leftItem release];
    
    
    
    
}
-(void)backClick
{
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableViewDataSoure

//- (float)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
//{
//    
//    return 124.0f;
//    
//}
//
//
//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
//{
//
//    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//    
//    button.frame = CGRectMake(10, 44, 100, 44);
//    
//    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    
//    [button setTitle:@"提交注册" forState:UIControlStateNormal];
//    
//    return button;
//    
//}

-(float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0)
    {
        return  40 + 20 + 30 + 44;
    }
    else if (indexPath.row == [self.cellName count] + 1)
    {
        return 124;
    }
    else
    {
        return 20 + 30 + 10 + 10 + 10;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [self.cellName count] + 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%d%d", [indexPath section], [indexPath row]];

    
    int row  =  indexPath.row;
    

    if (row  == 0)
    {
        UITableViewCell *cell  = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell)
        {
            
            cell = [[kata_UserRegistCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            
            
        }
        
        UITextField *phoneTextField = (UITextField *)[[cell.contentView viewWithTag:10] viewWithTag:2];
        
        _regInfo.handPhoneStr = phoneTextField.text;
        
        

        cell.selectionStyle  = UITableViewCellSelectionStyleNone;
        
        return cell;

    }else if (row == [self.cellName count] + 1)
        
    {
        
        UITableViewCell *cell  = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell)
        {
            
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            
            UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];

            button.frame = CGRectMake(100, 44, 100, 44);
            
            button.tag = 1;
            
            [button addTarget:self action:@selector(submitRegistBtn) forControlEvents:UIControlEventTouchUpInside];
        
            button.backgroundColor = [UIColor blueColor];
            
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

            [button setTitle:@"提交注册" forState:UIControlStateNormal];
            
            [cell.contentView addSubview:button];
    
            
        }
        
        
        

        cell.selectionStyle  = UITableViewCellSelectionStyleNone;

        return cell;
    }

    
    else
    {
        
        
        UITableViewCell *cell  = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell)
        {
            
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            
            UILabel *lab  = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 300, 20)];
            
            lab.tag  = 100;
            
            
            [cell.contentView addSubview:lab];
            
            
            [lab release];
            
            
            
            UITextField *textField  = [[UITextField alloc]initWithFrame:CGRectMake(10, lab.frame.origin.y + 20 + 10, 300, 30)];
            
            textField.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
            
            textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            
            textField.borderStyle  = UITextBorderStyleBezel;
            
            textField.tag  = row;
            
            textField.delegate = self;
            
            [cell.contentView addSubview:textField];
            
            [textField release];
            
            
        }
        
        UILabel *lab  = (UILabel *)[cell.contentView viewWithTag:100];
        
        lab.text  = [self.cellName objectAtIndex:(indexPath.row - 1)];
        
        
       UITextField *textField = (UITextField *) [cell.contentView viewWithTag:row];
        
        if (textField.tag == 2 || textField.tag == 3) {
            
            
            textField.secureTextEntry = YES;
        }
        
        
        
        if (row == 4)  //公司类型
        {
            
            for (UIView *view in cell.contentView.subviews)
            {
                if ([view isKindOfClass:[UITextField class]])
                {
                    [view removeFromSuperview];
                    
                    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                    
                    
                    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    
                    
                    [button setTitle:self.companyTypeStr forState:UIControlStateNormal];
                    
                    button.frame = CGRectMake(10, 30, 300, 40);
                    
                    button.tag = 11;
                    
                    [button addTarget:self action:@selector(companyTypeBtn) forControlEvents:UIControlEventTouchUpInside];
                    
                    [cell.contentView  addSubview:button];

                    
                }
                
            }
            
            
            UIButton *button  = (UIButton *)[cell.contentView viewWithTag:11];
            
            [button setTitle:self.companyTypeStr forState:UIControlStateNormal];
            
            _regInfo.companyTypeStr = self.companyTypeStr;
            
        }
        
        
        
        cell.selectionStyle  = UITableViewCellSelectionStyleNone;

        return cell;

        
        
    }
    
    
    
}

#pragma mark - tableViewDelegate

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
    {
        
        switch (textField.tag) {
        case 1:
        {
            _regInfo.verificationStr = textField.text;
            
        }
            break;
        case 2:
        {
            _regInfo.enterPsswordStr = textField.text;
            
        }
            break;
            
        case 3:
        {
            _regInfo.repetitionPsswordStr = textField.text;
            
        }
            break;
            
        case 4:  //公司类型
        {
            //                _regInfo.verificationStr = textField.text;
            
        }
            break;
            
        case 5:
        {
            _regInfo.companyNameStr = textField.text;
            
        }
            break;
            
        case 6:
        {
            _regInfo.companyAbbreviationStr = textField.text;
            
        }
            break;
            
        case 7:
        {
            _regInfo.companyPhoneStr = textField.text;
            
//          _regInfo.ZongStr =  [[_regInfo.companyPhoneStr substringFromIndex:4] copy];
            
            if ([_regInfo.companyPhoneStr length] > 4) {
                
                _regInfo.ZongStr =  [[_regInfo.companyPhoneStr substringToIndex:4] copy];
            }
            
            

            
        }
            break;
        case 8:
        {
            _regInfo.operationPeopleStr = textField.text;
            
        }
            break;
            
        case 9:
        {
            _regInfo.QQStr = textField.text;
            
        }
            break;
            
            
            
        default:
            break;
    }
    
        return YES;
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string

{
   
    if (textField.tag == 2 || textField.tag  == 3) {
        
        
        if (range.location >= 6 ) {
            
            return  NO;
            
        }
        
    
    }
    
    return YES;
    
}
//- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
//
//{
//    
//}



#pragma mark - textFiledDelegate


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];

    [textField endEditing:YES];
    
    return YES;
}

#pragma mark  - buttonClick

- (NSString *)sign
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    NSMutableString *sign = [NSMutableString stringWithString:@"10010 +"];
    
    [sign appendFormat:@"%@ + %ld",[self method],timestamp];
    
    [sign MD5];
    
    return [sign description] ;
}


- (NSString *)appkey
{
    return @"10010";
    
}
- (NSString *)method
{
    return @"user_register_post";
}

- (NSMutableDictionary *)params
{
    
    
    _postRegDic = [[[NSMutableDictionary alloc]initWithObjectsAndKeys:
                       _regInfo.handPhoneStr,@"username",
                       _regInfo.handPhoneStr,@"handphone",
                       [_regInfo.enterPsswordStr MD5],@"md5pass",
                       [NSString stringWithFormat:@"%d", _regInfo.companyType ],@"companyType",
                       _regInfo.companyNameStr,@"companyname",
                       _regInfo.companyAbbreviationStr,@"companybrief",
                       _regInfo.ZongStr,@"zong",
                       _regInfo.companyPhoneStr,@"companyphone",
                       _regInfo.operationPeopleStr,@"operator",
                       _regInfo.QQStr,@"qq",
                       _regInfo.verificationStr,@"randomcode",
                       nil] autorelease];
    
    
    return _postRegDic;
    
}

- (long int )time
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    return timestamp;
}

#pragma mark  - submitRegistBtn

- (void)submitRegistBtn
{
    
    DebugLog(@" %@  ==  %@",_regInfo.enterPsswordStr,_regInfo.repetitionPsswordStr);
    
    
    if (_regInfo.enterPsswordStr != nil && _regInfo.verificationStr != nil) {
        
        if ([_regInfo.enterPsswordStr isEqual:_regInfo.repetitionPsswordStr]) {
            
            

            NSString *urlStr  = [NSString stringWithFormat:@"%@",SERVER_URI];
            
            
            ASIFormDataRequest    *request  = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlStr]];
            
            [request setPostValue:[self sign] forKey:@"sign"];
            
            [request setPostValue:[self method] forKey:@"method"];
            
            [request setPostValue:[self appkey] forKey:@"appkey"];
            
            [request setPostValue:[self params] forKey:@"params"];
            
            [request setPostValue:[NSString stringWithFormat:@"%ld",[self time]] forKey:@"time"];
            
            request.timeOutSeconds  = 20;
            
            request.delegate = self;
            
            [request startAsynchronous];
            

            
        }
        else{
            
            UIAlertView *alert  = [[UIAlertView alloc]initWithTitle:@"温馨提示:" message:@"两次密码不一致。" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:@"取消", nil];
            
            [alert show];
            
            [alert release];
            
            
        }
        

        
        
    }
    
    
    
    
    
    
    
    
    
    
}
#pragma mark - ASIHttpDelegate

- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    DebugLog(@"request == %@",[request responseString]);
    
    
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:[request responseData] options:NSJSONReadingMutableLeaves error:nil];
    
    
    
    NSString *status = [dic objectForKey:@"status"];

    
    if ([status isEqualToString:@"OK"]) {  //注册成功
        
        NSString *code = [dic objectForKey:@"code"];
        
        NSDictionary *dataDic = [dic objectForKey:@"data"];
        
        NSString *dataCode = [dataDic objectForKey:@"code"];
        NSString *token = [dataDic objectForKey:@"token"];
        NSString *uid = [dataDic objectForKey:@"uid"];
        
        
        
        NSString *msg = [dic objectForKey:@"msg"];

        
        
        
        if ([self.delegate respondsToSelector:@selector(kata_RegistViewControllerHandPhone:)]) {
            
            [self.delegate kata_RegistViewControllerHandPhone:_regInfo.handPhoneStr];
            
            
        }
        
        
        [self.navigationController popViewControllerAnimated:YES];

    }
    
    
    
    
    
    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    DebugLog(@"requestFailedrequestFailed");

}




- (void)companyTypeBtn
{
    
    UIActionSheet  *sheet  = [[UIActionSheet alloc]initWithTitle:@"选择公司类型" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"实体",@"贸易",@"撮合", nil];
    
    [sheet showInView:self.view];
    
    [sheet release];
   
    
    
}




#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    DebugLog(@"buttonIndex =  %d",buttonIndex);
    
    
    switch (buttonIndex) {
        case 0:
        {
            
            
            self.companyTypeStr =  @"实体";
            
            _regInfo.companyType = 0;
            
            [self.tableView reloadData];
            
        }
            break;
        case 1:
        {
            self.companyTypeStr =  @"贸易";
            
            _regInfo.companyType = 1;

            
            [self.tableView reloadData];

        }
            break;
        case 2:
        {
            self.companyTypeStr =  @"撮合";
            
            _regInfo.companyType = 2;

            
            [self.tableView reloadData];


        }
            break;
            
        default:
            break;
    }
    
    
}


@end
