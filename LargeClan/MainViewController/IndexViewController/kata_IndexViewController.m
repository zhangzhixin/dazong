//
//  kata_IndexViewController.m
//  LargeClan
//
//  Created by kata on 13-11-19.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#define LABELWIDTH  120


#import "kata_IndexViewController.h"

#import "kata_testViewController.h"

#import "KATAUITabBarController.h"

#import "KATAUtils.h"

#import "kata_segView.h"

#import "ASIFormDataRequest.h"

#import "KATAConstants.h"

#import "NSString+MD5.h"

#import "KATAIndex.h"

@interface kata_IndexViewController ()
{
    UIScrollView *_scrollView;
    
    UITableView *_tableView1;
    UITableView *_tableView2;
    kata_segView *_segView;
    
    int _type;
}

@property(nonatomic,retain)NSMutableArray *dataInfo2;

@property(nonatomic,retain)NSDictionary *postDic;

@property(nonatomic,retain)NSArray *insideIndexArrayMEG; //默认


@property(nonatomic,retain)NSMutableArray *firstRowArrayInTableView2; //默认


@property(nonatomic,retain)NSArray *dataArray;

@property(nonatomic,retain)NSArray *array1;
@property(nonatomic,retain)NSArray *array2;

@property(nonatomic,retain)NSArray *rightNameArray;

@end

@implementation kata_IndexViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        
        UIImage* img1 =[UIImage imageNamed:@"edit.png"];
        UIImage* img2 =[UIImage imageNamed:@"edit_active.png"];
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        btn.frame =CGRectMake(0, 0, 32, 32);
        
        [btn setBackgroundImage:img1 forState:UIControlStateNormal];
        
        [btn setBackgroundImage:img2 forState:UIControlStateHighlighted];
        
        
        [btn addTarget: self action: @selector(editButtonClick) forControlEvents: UIControlEventTouchUpInside];
        
        UIBarButtonItem* item=[[UIBarButtonItem alloc]initWithCustomView:btn];
        
        self.navigationItem.rightBarButtonItem = item;
        
        [item release];
        
    }
    return self;
}

-(void)dealloc
{
    [_dataInfo2 release];
    
    [_firstRowArrayInTableView2 release];
    
    _segView.delegate = nil;
    [_segView release];

    [super dealloc];
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    
    KATAUITabBarController *tab  = (KATAUITabBarController  *)self.tabBarController;
    
    [tab showTabBar];
    
    _dataInfo2 = [[NSMutableArray alloc]init];

    [self sendRequest];
  
}

- (void)sendRequest
{
    NSString *urlStr  = [NSString stringWithFormat:@"%@",SERVER_URI];
    
    ASIFormDataRequest    *request  = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlStr]];
    
    [request setPostValue:[self sign] forKey:@"sign"];
    
    [request setPostValue:[self method] forKey:@"method"];
    
    [request setPostValue:[self appkey] forKey:@"appkey"];
    
    [request setPostValue:[self params] forKey:@"params"];
    
    [request setPostValue:[NSString stringWithFormat:@"%ld",[self time]] forKey:@"time"];
    
    request.timeOutSeconds  = 20;
    
    request.delegate = self;
    
    [request startAsynchronous];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"查询报盘";
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"header.png"] forBarMetrics:UIBarMetricsDefault];

    
   _segView = [[kata_segView alloc]initWithFrame:CGRectMake(0, 0, 320, 40)];
    
    _segView.delegate = self;

    [self.view addSubview:_segView];
        
//    self.array1 = @[@"a",@"b",@"c",@"d",@"e",@"f",@"g",@"h",@"j",@"k",@"l",@"A",@"b",@"c",@"d",@"e",@"f",@"g",@"h",@"j",@"k",@"l"];
//    self.array2 = @[@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"B",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11"];
//

    
//    self.insideIndexArrayMEG = [[NSMutableArray alloc]initWithObjects:@"方向",@"人民币",@"货种",@"交割时间",@"交割地",@"保证金",@"数量（吨）",@"报盘人",@"发票",@"免仓",@"状态",@"备注", nil];

    
    _firstRowArrayInTableView2  =  [[NSMutableArray alloc]initWithObjects:@"方向",@"人民币",@"货种",@"交割时间",@"交割地",@"保证金",@"数量（吨）",@"报盘人",@"发票",@"免仓",@"状态",@"备注", nil];
    
    
    
    _tableView1  = [[UITableView alloc]initWithFrame:CGRectMake(0, 44, 140, ScreenHeight) style:UITableViewStylePlain];
    
    _tableView1.alwaysBounceHorizontal = NO;
    _tableView1.alwaysBounceVertical = NO;

//    _tableView1.backgroundColor = [UIColor redColor];
    
    _tableView1.delegate =self;
    
    _tableView1.dataSource = self;
    
    
    [self.view addSubview:_tableView1];
    
    
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(140, 44, 220, ScreenHeight)];
    
    _scrollView.contentSize = CGSizeMake([_firstRowArrayInTableView2 count] * LABELWIDTH, 0);
    
    [self.view addSubview:_scrollView];
    
    
//    _tableView2  = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, [self.insideIndexArrayMEG count] * 80, ScreenHeight) style:UITableViewStylePlain];
//    
    
    _tableView2  = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, [_firstRowArrayInTableView2 count] * LABELWIDTH, ScreenHeight) style:UITableViewStylePlain];
    

    _tableView2.delegate =self;
    
    _tableView2.dataSource = self;
    
    
    _tableView2.alwaysBounceHorizontal = NO;
    _tableView2.alwaysBounceVertical = NO;
    
    [_scrollView addSubview:_tableView2];
    
}

#pragma mark - tableViewDelegate

/*
 *
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    if (tableView == _tableView1) {

        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 120, 44)];
        
//     view.backgroundColor = [UIColor redColor];

        UILabel *lab1 = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 60, 44)];
        
        lab1.backgroundColor = [UIColor clearColor];
    
        lab1.text = @"报盘时间";
        
        [view addSubview:lab1];
        
        [lab1 release];
        
        UILabel *lab2 = [[UILabel alloc]initWithFrame:CGRectMake(60, 0, 60, 44)];
        
        lab2.text = @"报盘编号";

        lab2.backgroundColor = [UIColor clearColor];

        [view addSubview:lab2];
        
        [lab2 release];
        
        return view;
        
    }else
    {
        
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [self.insideIndexArrayMEG count] * 80, 44)];
        
        view.backgroundColor = [UIColor redColor];
        
        return view;
        

        
    }
}

*/

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
       return [self.dataInfo2 count] + 1;
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (tableView == _tableView1)
    {
        
        static NSString *cellIdentifier1 = @"Cell1";

        UITableViewCell *cell1 = [tableView dequeueReusableCellWithIdentifier:cellIdentifier1];
        
        
        if (!cell1)
        {
            
            cell1 = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier1] autorelease];
            
            
            UIImageView *imageView1 = [[UIImageView alloc]initWithFrame:CGRectMake(0, 1, 20, 20)];

//            imageView1.backgroundColor = [UIColor greenColor];
            
            imageView1.tag = 11;
            
            [cell1 addSubview:imageView1];
            
            [imageView1 release];
            
            
            UIImageView *imageView2 = [[UIImageView alloc]initWithFrame:CGRectMake(0, 22, 20, 20)];
            
            
//            imageView2.backgroundColor = [UIColor blueColor];

            imageView2.tag = 12;

            [cell1 addSubview:imageView2];
            
            [imageView2 release];

            
            
            
            
            UILabel *timeLab = [[UILabel alloc]initWithFrame:CGRectMake(20, 0, 60, 44)];
            
            timeLab.tag =111;
            
            
//            timeLab.backgroundColor = [UIColor redColor];
            
//            timeLab.adjustsFontSizeToFitWidth = YES;
            
            [cell1 addSubview:timeLab];
            
            [timeLab release];
            
            UILabel *numberLab = [[UILabel alloc]initWithFrame:CGRectMake(80, 0, 60, 44)];
            
            numberLab.tag = 112;
            
//            [numberLab setFont:[UIFont systemFontOfSize:12.0f]];
        
//            numberLab.backgroundColor = [UIColor greenColor];

//            numberLab.adjustsFontSizeToFitWidth = YES;
            
            [cell1 addSubview:numberLab];
            
            [numberLab release];
            
        }
        
        UILabel *nameLab = (UILabel *)[cell1 viewWithTag:111];
        
        UILabel *numberLab = (UILabel *)[cell1 viewWithTag:112];

        
        if (indexPath.row == 0)
        {
            
            
            [nameLab setFont:[UIFont systemFontOfSize:15.0f]];
            nameLab.text = @"报盘时间";
            
            
            [numberLab setFont:[UIFont  systemFontOfSize:15.0f]];

            numberLab.text =  @"报盘编号";

    
        }
        else
        {
            
            
            KATAIndex *indexObj = [self.dataInfo2 objectAtIndex:indexPath.row -1];

            //时间
            nameLab.text  = indexObj.auditingstatus;
            
            [nameLab setFont:[UIFont systemFontOfSize:10.0f]];

            //报盘编号
            
            [numberLab setFont:[UIFont  systemFontOfSize:8.0f]];
            
            numberLab.text  = indexObj.code;
            
            UIImageView *imageView1 = (UIImageView *)[cell1 viewWithTag:11];
            
            
            if ([indexObj.notfaze isEqualToString:@"0"]) {
                
                [imageView1 setImage:[UIImage imageNamed:@"w.png"]];
                
            }else
            {

                imageView1.backgroundColor = [UIColor clearColor];
                
            }
            
            //            indexObj.notfaze = [NSString stringWithFormat:@"%d",notfaze];
            
            
            UIImageView *imageView2 = (UIImageView *)[cell1 viewWithTag:12];
            

            [imageView2 setImage:[UIImage imageNamed:@"rz.png"]];
            
        }
        
        cell1.selectionStyle = UITableViewCellSelectionStyleNone;

        return cell1;
    }
    
    else  //表 2
    {
        
        static NSString *CellInside = @"CellInside";

        
        static NSString *CellOut = @"CellOut";
        
        static NSString *CellInsidePTA = @"CellInsidePTA";

        static NSString *CellOutPTA = @"CellOutPTA";

        
        int row = indexPath.row;

        if ([_firstRowArrayInTableView2 count] == 12) {  //MEG内盘
            
            UITableViewCell *cell2Inside = [tableView dequeueReusableCellWithIdentifier:CellInside];
            
            
            if (!cell2Inside)
            {
                
                cell2Inside = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellInside] autorelease];
                
                for (int  i = 0; i < [_firstRowArrayInTableView2  count];  i ++)
                {
                    
                    UILabel *lab  = [[UILabel alloc]initWithFrame:CGRectMake(i * LABELWIDTH, 0, LABELWIDTH, 44)];
                    
                    [lab setFont:[UIFont systemFontOfSize:14]];
                    
                    lab.tag =  i + 10;
                    
                    lab.textAlignment = NSTextAlignmentCenter;
                    
                    [cell2Inside addSubview:lab];
                    
                    [lab release];
                    
                    if (lab.tag == 17) {
                        
                        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake((120 - 44)/2, 11, 22, 22)];
                        
                        imageView.tag = 222;
                        
//                        imageView.backgroundColor = [UIColor redColor];
                        
                        [lab addSubview:imageView];
                        
                        [imageView release];
                        

                    }
                    

                }
                
                
            }
            
            
                if (row == 0)
                {
                    
                    
                    for (int  i = 0; i < [_firstRowArrayInTableView2  count];  i ++)
                    {
                        
                        UILabel *rightNameLab = (UILabel *)[cell2Inside viewWithTag: i + 10];
                        
                        rightNameLab.text = [_firstRowArrayInTableView2 objectAtIndex:i];
                        
                    }
                    
                }else {    //MEG内盘
                    
                    
                    KATAIndex *indexObj = [_dataInfo2 objectAtIndex:row - 1];
                    
                    UIImageView *imageView  = (UIImageView *)[cell2Inside viewWithTag:222];
                    
                    [imageView setImage:[UIImage imageNamed:@"kefu.png"]];
                    
                    UILabel *directionLab = (UILabel *)[cell2Inside viewWithTag:10];
                    
                    directionLab.text = indexObj.direction;
                    
                    //人民币
                    UILabel *quotationLab = (UILabel *)[cell2Inside viewWithTag:11];
                    
                    quotationLab.text  = indexObj.quotation;
                    
                    //货种
                    UILabel *spotfuturesLab = (UILabel *)[cell2Inside viewWithTag:12];
                    
                    spotfuturesLab.text = indexObj.spotfutures;
                    
                    
                    //                    //交割时间
                    UILabel *deliveryLab = (UILabel *)[cell2Inside viewWithTag:13];
                    
                    deliveryLab.text = indexObj.deliverytime;
                    
                    //                    //交割地
                    UILabel *deliverynameLab = (UILabel *)[cell2Inside viewWithTag:14];
                    
                    deliverynameLab.text = indexObj.deliveryname;
        
                    
                    //保证金
                    UILabel *cautionmoneyLab = (UILabel *)[cell2Inside viewWithTag:15];
                    
                    cautionmoneyLab.text = indexObj.cautionmoney;
                    
                    
                    //数量
                    UILabel *numberLab = (UILabel *)[cell2Inside viewWithTag:16];
                    
                    numberLab.text = indexObj.number;
                    
                    //报盘人
                    //                    UILabel *moneyLab = (UILabel *)[cell2 viewWithTag:17];
                    
                    //发票
                    UILabel *invoiceLab = (UILabel *)[cell2Inside viewWithTag:18];
                    
                    invoiceLab.text = indexObj.invoice;
                    
                    //免仓
                    UILabel *storagetimeLab = (UILabel *)[cell2Inside viewWithTag:19];
                    storagetimeLab.text = indexObj.storagetime;
                    
                    //                    //状态
                    UILabel *offerstateLab = (UILabel *)[cell2Inside viewWithTag:20];
                    
                    offerstateLab.text = indexObj.state;
                    
                    //                    //备注
                    UILabel *remarksLab = (UILabel *)[cell2Inside viewWithTag:21];
                    
                    remarksLab.text  = indexObj.remarks;

                }
                
                
            
                cell2Inside.selectionStyle = UITableViewCellSelectionStyleNone;
                
                return cell2Inside;


        }else if ([_firstRowArrayInTableView2 count] == 15) {  //MEG 外盘 //MEG 外盘
        
            UITableViewCell *cell2Out = [tableView dequeueReusableCellWithIdentifier:CellOut];
            
            
        if (!cell2Out)
            {
                
                cell2Out = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellOut] autorelease];
                
                for (int  i = 0; i < [_firstRowArrayInTableView2  count];  i ++)
                {
                    
                    UILabel *lab  = [[UILabel alloc]initWithFrame:CGRectMake(i * LABELWIDTH, 0, LABELWIDTH, 44)];
                    
                    [lab setFont:[UIFont systemFontOfSize:14]];
                    
                    lab.tag =  i + 10;
                    
                    lab.textAlignment = NSTextAlignmentCenter;

                    [cell2Out addSubview:lab];
                    
                    [lab release];
                    
                }
            }
            
            
            
            if (row == 0)
            {
                
                
                for (int  i = 0; i < [_firstRowArrayInTableView2  count];  i ++)
                {
                    
                    UILabel *rightNameLab = (UILabel *)[cell2Out viewWithTag: i + 10];
                    
                    rightNameLab.text = [_firstRowArrayInTableView2 objectAtIndex:i];
                    
                }
                
            }else{ //MEG 外盘 //MEG 外盘
                
                
                
                KATAIndex *indexObj = [_dataInfo2 objectAtIndex:row - 1];
                
                UILabel *directionLab = (UILabel *)[cell2Out viewWithTag:10];
                
                directionLab.text = indexObj.direction;
                
                //美金
                UILabel *quotationLab = (UILabel *)[cell2Out viewWithTag:11];
                
                quotationLab.text  = indexObj.quotation;
                
                //货种
                UILabel *spotfuturesLab = (UILabel *)[cell2Out viewWithTag:12];
                
                spotfuturesLab.text = indexObj.spotfutures;
                
                
                //                    //货源地
                UILabel *goodssourcenameLab = (UILabel *)[cell2Out viewWithTag:13];
                
                goodssourcenameLab.text = indexObj.goodssourcename;
                
                
                //                    //预计到港时间  开始
                
                UILabel *loadingtime1Lab = (UILabel *)[cell2Out viewWithTag:14];
                
                loadingtime1Lab.text = indexObj.loadingtime1;
                
              
                //                    //预计到港时间 结束
                
                UILabel *loadingtime2Lab = (UILabel *)[cell2Out viewWithTag:15];
                
                loadingtime2Lab.text = indexObj.loadingtime2;
                
                
                
                //转手
                UILabel *changehandsLab = (UILabel *)[cell2Out viewWithTag:16];
                
                changehandsLab.text = indexObj.changehands;
                
                //交单
                    UILabel *surrenderdocumentsLab = (UILabel *)[cell2Out viewWithTag:17];
                
                surrenderdocumentsLab.text = indexObj.surrenderdocuments;

                //交割地
                UILabel *regionLab = (UILabel *)[cell2Out viewWithTag:18];
                
                regionLab.text = indexObj.deliveryname;
                
                //数量
                UILabel *numberLab = (UILabel *)[cell2Out viewWithTag:19];
                
                numberLab.text = indexObj.number;
                
                //                   报盘人
//                UILabel *offerstateLab = (UILabel *)[cell2Out viewWithTag:20];
                
//                offerstateLab.text = indexObj.offerstate;
                
                //                    //清单
                
                UILabel *detailedlistLab = (UILabel *)[cell2Out viewWithTag:21];
                
                detailedlistLab.text  = indexObj.detailedlist;

                //                    //付款方式
                UILabel *paymentmethodsLab = (UILabel *)[cell2Out viewWithTag:22];
                
                paymentmethodsLab.text  = indexObj.paymentmethods;

                //                    //状态
                UILabel *offerstateLab = (UILabel *)[cell2Out viewWithTag:23];
                
                offerstateLab.text  = indexObj.state;

                
                //                    //备注
                UILabel *remarksLab = (UILabel *)[cell2Out viewWithTag:24];
                
                remarksLab.text  = indexObj.remarks;

            }
            
            
            
           
            

            cell2Out.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell2Out;

        }else if ([_firstRowArrayInTableView2 count] == 13) {
            
            UITableViewCell *cellInsidePTA = [tableView dequeueReusableCellWithIdentifier:CellInsidePTA];
            
            
            if (!cellInsidePTA)
            {
                
                cellInsidePTA = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellInsidePTA] autorelease];
                
                for (int  i = 0; i < [_firstRowArrayInTableView2  count];  i ++)
                {
                    
                    UILabel *lab  = [[UILabel alloc]initWithFrame:CGRectMake(i * LABELWIDTH, 0, LABELWIDTH, 44)];
                    
                    [lab setFont:[UIFont systemFontOfSize:14]];
                    
                    lab.tag =  i + 10;
                    
                    
                    lab.textAlignment = NSTextAlignmentCenter;

                    [cellInsidePTA addSubview:lab];
                    
                    [lab release];
                    
                }
            }
            
            
            
            if (row == 0)
            {
                
                
                for (int  i = 0; i < [_firstRowArrayInTableView2  count];  i ++)
                {
                    
                    UILabel *rightNameLab = (UILabel *)[cellInsidePTA viewWithTag: i + 10];
                    
                    rightNameLab.text = [_firstRowArrayInTableView2 objectAtIndex:i];
                    
                }
                
            }else{
                
                
                KATAIndex *indexObj = [_dataInfo2 objectAtIndex:row - 1];
                
                UILabel *directionLab = (UILabel *)[cellInsidePTA viewWithTag:10];
                
                directionLab.text = indexObj.direction;
                
                //人民币
                UILabel *quotationLab = (UILabel *)[cellInsidePTA viewWithTag:11];
                
                quotationLab.text  = indexObj.quotation;
                
                //货种
                UILabel *spotfuturesLab = (UILabel *)[cellInsidePTA viewWithTag:12];
                
                spotfuturesLab.text = indexObj.spotfutures;
                
                
                //                    //交割时间
                UILabel *deliveryLab = (UILabel *)[cellInsidePTA viewWithTag:13];
                
                deliveryLab.text = indexObj.deliverytime;
                
                //                    //交割地
                UILabel *deliverynameLab = (UILabel *)[cellInsidePTA viewWithTag:14];
                
                deliverynameLab.text = indexObj.deliveryname;
                //保证金
                UILabel *cautionmoneyLab = (UILabel *)[cellInsidePTA viewWithTag:15];
                
                cautionmoneyLab.text = indexObj.cautionmoney;
                
                
                //数量
                UILabel *numberLab = (UILabel *)[cellInsidePTA viewWithTag:16];
                
                numberLab.text = indexObj.number;
                
                //报盘人
                //                    UILabel *moneyLab = (UILabel *)[cell2 viewWithTag:17];
                
                //发票
                UILabel *invoiceLab = (UILabel *)[cellInsidePTA viewWithTag:18];
                
                invoiceLab.text = indexObj.invoice;
                
                //免仓
                UILabel *storagetimeLab = (UILabel *)[cellInsidePTA viewWithTag:19];
                storagetimeLab.text = indexObj.storagetime;
                
                //                    //状态
                UILabel *offerstateLab = (UILabel *)[cellInsidePTA viewWithTag:20];
                
                offerstateLab.text = indexObj.offerstate;
                
                //                    //备注
                UILabel *remarksLab = (UILabel *)[cellInsidePTA viewWithTag:21];
                
                remarksLab.text  = indexObj.remarks;

                
                
            }
            
            
            
            
            
            cellInsidePTA.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cellInsidePTA;
            
        }else if ([_firstRowArrayInTableView2 count] == 16) {
            
            UITableViewCell *cellOutPTA = [tableView dequeueReusableCellWithIdentifier:CellOutPTA];
            
            
            if (!cellOutPTA)
            {
                
                cellOutPTA = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellOutPTA] autorelease];
                
                for (int  i = 0; i < [_firstRowArrayInTableView2  count];  i ++)
                {
                    
                    UILabel *lab  = [[UILabel alloc]initWithFrame:CGRectMake(i * LABELWIDTH, 0, LABELWIDTH, 44)];
                    
                    [lab setFont:[UIFont systemFontOfSize:14]];
                    
                    lab.tag =  i + 10;
                    
                    
                    lab.textAlignment = NSTextAlignmentCenter;

                    [cellOutPTA addSubview:lab];
                    
                    [lab release];
                    
                }
            }
            
            
            
            if (row == 0)
            {
                
                
                for (int  i = 0; i < [_firstRowArrayInTableView2  count];  i ++)
                {
                    
                    UILabel *rightNameLab = (UILabel *)[cellOutPTA viewWithTag: i + 10];
                    
                    rightNameLab.text = [_firstRowArrayInTableView2 objectAtIndex:i];
                    
                }
                
            }
            
            
            
            
            
            
            cellOutPTA.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cellOutPTA;
            
        }
        

        
        

        
    }
    
        
//        UITableViewCell *cell2 = [tableView dequeueReusableCellWithIdentifier:cellIdentifier2];
        
        
        
        /*
         
         *
        
        if (!cell2)
        {
        
            cell2 = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier2] autorelease];
            for (int  i = 0; i < [_firstRowArrayInTableView2  count];  i ++)
                {

                    UILabel *lab  = [[UILabel alloc]initWithFrame:CGRectMake(i * 80, 0, 80, 44)];

                    [lab setFont:[UIFont systemFontOfSize:14]];

                    lab.tag =  i + 10;

                    [cell2 addSubview:lab];
                    
                    [lab release];
                    
                }

           
//            if (self.dataArray != nil)
//            {
//                for (int  i = 0; i < [self.dataArray  count];  i ++)
//                {
//                    
//                    UILabel *lab  = [[UILabel alloc]initWithFrame:CGRectMake(i * 80, 0, 80, 44)];
//                    
//                    [lab setFont:[UIFont systemFontOfSize:14]];
//                    
//                    lab.tag =  i + 10;
//                    
//                    [cell2 addSubview:lab];
//                    
//                    [lab release];
//                    
//                }
//                
//                
//                
//                
//
//            }else //默认
//            {
//                for (int  i = 0; i < [self.insideIndexArrayMEG  count];  i ++)
//                {
//                    
//                    UILabel *lab  = [[UILabel alloc]initWithFrame:CGRectMake(i * 80, 0, 80, 44)];
//                    
//                    [lab setFont:[UIFont systemFontOfSize:14]];
//                    
//                    lab.tag =  i + 10;
//                    
//                    [cell2 addSubview:lab];
//                    
//                    [lab release];
//                    
//                }
//
//            }
            
        }
    
        if (row == 0)
        {
            
            
            for (int  i = 0; i < [_firstRowArrayInTableView2  count];  i ++)
            {

                UILabel *rightNameLab = (UILabel *)[cell2 viewWithTag: i + 10];

                rightNameLab.text = [_firstRowArrayInTableView2 objectAtIndex:i];

            }

            
            
            
            
            
            
            
            //            if (self.dataArray != nil)
//                {
//                 
//                    for (int  i = 0; i < [self.dataArray  count];  i ++)
//                    {
//                        
//                        UILabel *rightNameLab = (UILabel *)[cell2 viewWithTag: i + 10];
//                        
//                        
//                    
//                        
//                        rightNameLab.text = [self.dataArray objectAtIndex:i];
//
//                    }
//
//                }
//                else
//                {
//                    for (int  i = 0; i < [self.insideIndexArrayMEG  count];  i ++)
//                    {
//                        
//                        UILabel *rightNameLab = (UILabel *)[cell2 viewWithTag: i + 10];
//                        
//                        
//                        rightNameLab.text = [self.insideIndexArrayMEG objectAtIndex:i];
//                        
//                    }
//
//                }
            
        }
        else
        {
            
            if (self.dataArray != nil)
            {
                
                KATAIndex *indexObj = [_dataInfo2 objectAtIndex:row - 1];
                
                UILabel *directionLab = (UILabel *)[cell2 viewWithTag:10];
                
                directionLab.text = indexObj.direction;
                
                //人民币
                UILabel *quotationLab = (UILabel *)[cell2 viewWithTag:11];
                
                quotationLab.text  = indexObj.quotation;
                
                //货种
                UILabel *spotfuturesLab = (UILabel *)[cell2 viewWithTag:12];
                
                spotfuturesLab.text = indexObj.spotfutures;
                
                
                //                    //交割时间
                UILabel *deliveryLab = (UILabel *)[cell2 viewWithTag:13];
                
                deliveryLab.text = indexObj.deliverytime;
                
                //                    //交割地
                UILabel *deliverynameLab = (UILabel *)[cell2 viewWithTag:14];
                
                deliverynameLab.text = indexObj.deliveryname;
                //保证金
                UILabel *cautionmoneyLab = (UILabel *)[cell2 viewWithTag:15];
                
                cautionmoneyLab.text = indexObj.cautionmoney;
                
                
                //数量
                UILabel *numberLab = (UILabel *)[cell2 viewWithTag:16];
                
                numberLab.text = indexObj.number;
                
                //报盘人
                //                    UILabel *moneyLab = (UILabel *)[cell2 viewWithTag:17];
                
                //发票
                UILabel *invoiceLab = (UILabel *)[cell2 viewWithTag:18];
                
                invoiceLab.text = indexObj.invoice;
                
                //免仓
                UILabel *storagetimeLab = (UILabel *)[cell2 viewWithTag:19];
                storagetimeLab.text = indexObj.storagetime;
                
                //                    //状态
                UILabel *offerstateLab = (UILabel *)[cell2 viewWithTag:20];
                
                offerstateLab.text = indexObj.offerstate;
                
                //                    //备注
                UILabel *remarksLab = (UILabel *)[cell2 viewWithTag:21];
                
                remarksLab.text  = indexObj.remarks;
                
            
            
            }
            else
            {
                if ([_dataInfo2 count] != 0) {
                    
                    KATAIndex *indexObj = [_dataInfo2 objectAtIndex:row - 1];

                    UILabel *directionLab = (UILabel *)[cell2 viewWithTag:10];
                    
                    directionLab.text = indexObj.direction;
                    
                    //人民币
                    UILabel *quotationLab = (UILabel *)[cell2 viewWithTag:11];
                    
                    quotationLab.text  = indexObj.quotation;
                
                    //货种
                    UILabel *spotfuturesLab = (UILabel *)[cell2 viewWithTag:12];
                    
                    spotfuturesLab.text = indexObj.spotfutures;
                    
                    
//                    //交割时间
                    UILabel *deliveryLab = (UILabel *)[cell2 viewWithTag:13];
                    
                    deliveryLab.text = indexObj.deliverytime;
                    
//                    //交割地
                    UILabel *deliverynameLab = (UILabel *)[cell2 viewWithTag:14];
                    
                    deliverynameLab.text = indexObj.deliveryname;
                    //保证金
                    UILabel *cautionmoneyLab = (UILabel *)[cell2 viewWithTag:15];
                    
                    cautionmoneyLab.text = indexObj.cautionmoney;
                    
                    
                    //数量
                    UILabel *numberLab = (UILabel *)[cell2 viewWithTag:16];
                    
                    numberLab.text = indexObj.number;
                    
                    //报盘人
//                    UILabel *moneyLab = (UILabel *)[cell2 viewWithTag:17];

                    //发票
                    UILabel *invoiceLab = (UILabel *)[cell2 viewWithTag:18];
                    
                    invoiceLab.text = indexObj.invoice;

                    //免仓
                    UILabel *storagetimeLab = (UILabel *)[cell2 viewWithTag:19];
                    storagetimeLab.text = indexObj.storagetime;
                    
//                    //状态
                    UILabel *offerstateLab = (UILabel *)[cell2 viewWithTag:20];
                    
                    offerstateLab.text = indexObj.offerstate;
                    
//                    //备注
                    UILabel *remarksLab = (UILabel *)[cell2 viewWithTag:21];
                
                    remarksLab.text  = indexObj.remarks;
                    

                }
                
//                for (int  i = 0; i < [self.insideIndexArrayMEG  count];  i ++)
//                {
//                    
//                    UILabel *rightNameLab = (UILabel *)[cell2 viewWithTag: i + 10];
//                    
//                    // rightNameLab.text = [self.insideIndexArrayMEG objectAtIndex:i];
//                    
//                    
//                }
                
                
            }

            
            
            
        }
        
        

        
        cell2.selectionStyle = UITableViewCellSelectionStyleNone;

        return cell2;
    }
    
         
         */
        
        
        
        
    
    
}


#pragma mark -uiscrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    _tableView1.contentOffset =  scrollView.contentOffset;
    
    _tableView2.contentOffset =   _tableView1.contentOffset;
    
    
}


#pragma mark -custom

-(void)buttonClick:(id)sender
{
    
    kata_testViewController *testVC = [[kata_testViewController alloc]initWithNibName:nil bundle:nil];
    

    testVC.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:testVC animated:YES];
    
    [testVC release];
    
    
}

#pragma mark - segDelegate
-(void)changeIndexNameData:(NSArray *)array
{
    
    [_firstRowArrayInTableView2 removeAllObjects];
    
//    _firstRowArrayInTableView2 =  (NSMutableArray *)array;
    
    
    [_firstRowArrayInTableView2 addObjectsFromArray:array];
    
    _scrollView.contentSize = CGSizeMake([_firstRowArrayInTableView2 count] * LABELWIDTH, 0);
    
    _tableView2.frame = CGRectMake(0, 0, [_firstRowArrayInTableView2 count] * LABELWIDTH, ScreenHeight);

    
    [_tableView2 reloadData];
    
}

- (void)changeIndex:(int)index
{
    
    _type = index - 1;
    
    
    [self sendRequest];
    
    
    
    
    
}
#pragma mark - sign

- (NSString *)sign
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    NSMutableString *sign = [NSMutableString stringWithString:@"10010 +"];
    
    [sign appendFormat:@"%@ + %ld",[self method],timestamp];
    
    [sign MD5];
    
    return [sign description] ;
}


- (NSString *)appkey
{
    return @"10010";
    
}
- (NSString *)method
{
    return @"index_offer_get";
}

- (NSDictionary *)params
{
    
    
    NSString *nameofpart = [NSString stringWithFormat:@"%d",_type];
    
    
    _postDic = [[[NSMutableDictionary alloc]initWithObjectsAndKeys:
                   nameofpart,@"nameofpart",
                 
                    nil] autorelease];
    
    
    return _postDic;
    
}
- (long int )time
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    return timestamp;
}

#pragma mark - ASIHttpDelegate

- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    [_dataInfo2 removeAllObjects];
    
    
    DebugLog(@"request == %@",[request responseString]);
    
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:[request responseData] options:NSJSONReadingMutableLeaves error:nil];
    
    
    NSString *code = [dic objectForKey:@"code"];
    
    NSDictionary *data = [dic objectForKey:@"data"];
    
    NSString *dataCode = [data objectForKey:@"code"];

    NSArray *offer_results = [data objectForKey:@"offer_results"];

    for (int i = 0; i < [offer_results count]; i++) {
        
        KATAIndex *indexObj = [[KATAIndex alloc]init];
        
        NSDictionary *dic = [offer_results objectAtIndex:i];
        
    
        //报盘时间
        
          long long time = [[[dic objectForKey:@"auditingDate"] objectForKey:@"time"] longLongValue];

        NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:(time / 1000)];
        
//        NSLog(@"confromTimesp ==  %@",confromTimesp);

    
        indexObj.auditingstatus = [self date:confromTimesp];

        
        

        
        //交割时间

        if ([dic objectForKey:@"deliverytime2"]  != [NSNull null] && [dic objectForKey:@"deliverytime2"] != [NSNull null]) {

        
            
            long long deliveryTime1 = [[[dic objectForKey:@"deliverytime1"] objectForKey:@"time"] longLongValue];
            
            NSDate *confromTimesp1 = [NSDate dateWithTimeIntervalSince1970:(deliveryTime1 / 1000)];
            
            NSLog(@"confromTimesp1 ==  %@",confromTimesp1);
            
            indexObj.deliverytime1 = [self deliveryDate:confromTimesp1];
            
            
            long long deliveryTime2 = [[[dic objectForKey:@"deliverytime2"] objectForKey:@"time"] longLongValue];
            
            NSDate *confromTimesp2 = [NSDate dateWithTimeIntervalSince1970:(deliveryTime2 / 1000)];
            
            NSLog(@"confromTimesp2 ==  %@",confromTimesp2);
            
            indexObj.deliverytime2 = [self deliveryDate:confromTimesp2];
            
            NSString *deliveryStr1  = [indexObj.deliverytime1 stringByReplacingOccurrencesOfString:@"GMT+" withString:@"-"];
            
            NSString *deliveryStr2  = [indexObj.deliverytime2 stringByReplacingOccurrencesOfString:@"GMT+" withString:@""];
            
            
            indexObj.deliverytime = (NSMutableString *)[deliveryStr1 stringByAppendingString:deliveryStr2];
            
            DebugLog(@"indexObj.deliverytime ==  %@",indexObj.deliverytime);
            
            
        }
       
        
        //预计装船开始时间
        if ([dic objectForKey:@"loadingtime1"]  != [NSNull null]) {
            
            
            
            long long loadingtime1 = [[[dic objectForKey:@"loadingtime1"] objectForKey:@"time"] longLongValue];
            
            NSDate *confromTimesp1 = [NSDate dateWithTimeIntervalSince1970:(loadingtime1 / 1000)];
            
            NSLog(@"confromTimesp1 ==  %@",confromTimesp1);
            
            indexObj.loadingtime1 = [[self deliveryDate:confromTimesp1] stringByReplacingOccurrencesOfString:@"GMT+" withString:@""];
        
        }

        //预计装船结束时间

        if ([dic objectForKey:@"loadingtime2"]  != [NSNull null]) {
            
            
            
            long long loadingtime2 = [[[dic objectForKey:@"loadingtime2"] objectForKey:@"time"] longLongValue];
            
            NSDate *confromTimesp1 = [NSDate dateWithTimeIntervalSince1970:(loadingtime2 / 1000)];
            
            NSLog(@"confromTimesp1 ==  %@",confromTimesp1);
            
            indexObj.loadingtime2 = [[self deliveryDate:confromTimesp1] stringByReplacingOccurrencesOfString:@"GMT+" withString:@""];
            
        }
//        
        //货源地
            
            if ( [dic objectForKey:@"goodsSource"] != [NSNull null]) {

         
                NSDictionary *goodsSource  =[dic objectForKey:@"goodsSource"];
                
                NSString *goodssourcename = [goodsSource objectForKey:@"goodssourcename"];
                
                indexObj.goodssourcename = goodssourcename;

                }
       //
//        
        //交割地
        
        if ( [dic objectForKey:@"region"] != [NSNull null]) {
            
            NSDictionary *regionDic  = [dic objectForKey:@"region"];
            
            NSString *deliveryname = [regionDic objectForKey:@"deliveryname"];
            
            indexObj.deliveryname = deliveryname;
            

        }
//
        
        
        // 转手
        if ( [dic objectForKey:@"changehands"] != [NSNull null]) {
            
            int  changehands =  [[dic objectForKey:@"changehands"] intValue];
            
            DebugLog(@"changehands = %d",changehands);

                indexObj.changehands = [NSString stringWithFormat:@"%d手",changehands];
            
            DebugLog(@"indexObj.changehands = %@",indexObj.changehands);
        }
        
        // 交单天数
        
        if ( [dic objectForKey:@"surrenderdocuments"] != [NSNull null]) {
            
            int  surrenderdocuments =  [[dic objectForKey:@"surrenderdocuments"] intValue];
            
            indexObj.surrenderdocuments = [NSString stringWithFormat:@"%d天",surrenderdocuments];
            
        }

        // 付款方式
        
        if ( [dic objectForKey:@"paymentmethods"] != [NSNull null]) {
            
            int  paymentmethods =  [[dic objectForKey:@"paymentmethods"] intValue];
            
            
            if (paymentmethods == 0) {
                
                indexObj.paymentmethods = @"L/C90 天";
                
            }else if (paymentmethods == 1) {
                
                indexObj.paymentmethods = @"L/C 即期";
                
            }
            else if (paymentmethods == 2) {
                
                indexObj.paymentmethods = @"L/T ";
                
            }
            else{
                indexObj.paymentmethods = @"其他";
                
            }
            

        
        }
        
        // 单据类型
        
        if ( [dic objectForKey:@"detailedlist"] != [NSNull null]) {
            
            int  detailedlist = [[dic objectForKey:@"detailedlist"] intValue];

            if (detailedlist == 0) {
                
                indexObj.detailedlist = @"是";
                
            }else
            {
                indexObj.detailedlist = @"否";
                
            }

        }


        
        
        
//        //价格
        
        NSString *quotation  = [NSString stringWithFormat:@"%d",[[dic objectForKey:@"quotation"] intValue]];

        indexObj.quotation = quotation;
        
        
        //方向
        int  direction =  [[dic objectForKey:@"direction"] intValue];
        
        DebugLog(@"directiondirection ==  %d",direction);
        
        if (direction == 0) {
            
            indexObj.direction = @"卖盘";
            
        }else
        {
            indexObj.direction = @"买盘";

        }
        
        
        
        NSString *megCode = [dic objectForKey:@"code"];
    
        indexObj.code = megCode;
        
        
        //有效期
        int offerstate = [[dic objectForKey:@"offerstate"] intValue];
        
        if (offerstate == 0) {
            
            indexObj.offerstate = @"有效";
            
        }else{
            
            indexObj.offerstate = @"过期";

        }
        //是否成交
        int dealstatus = [[dic objectForKey:@"dealstatus"] intValue];
        
        if (dealstatus == 0) {
            
            indexObj.dealstatus = @"/成交";
            
        }else{
            
            indexObj.dealstatus = @"/未成交";
            
        }
        
        
        NSMutableString *state = (NSMutableString *)[indexObj.offerstate  stringByAppendingString:indexObj.dealstatus];
        
        indexObj.state = state;
        
        

//
        
        //审核状态
        int auditingstatus = (int) [dic objectForKey:@"auditingstatus"];
        
              //是否删除
        BOOL isdeleted = (BOOL) [dic objectForKey:@"isdeleted"];

        //是否发布
        BOOL iscancel = (BOOL) [dic objectForKey:@"iscancel"];

        //品名
        int nameofpart = (int) [dic objectForKey:@"nameofpart"];
        
        //期货类型
        int spotfutures = (int) [dic objectForKey:@"spotfutures"];

        
        if (spotfutures == 0) {
            
            indexObj.spotfutures = @"期货";
            
        }else
        {
            indexObj.spotfutures = @"现货";
            
        }

        //吨数
        int number = (int) [dic objectForKey:@"number"];

        
        if (number == 0) {
            
            indexObj.number = @"500";
            
        }else  if (number == 1)
        {
            indexObj.spotfutures = @"1000";
            
        }
        else  if (number == 2)
        {
            indexObj.number = @"1500";
            
        }
        else  if (number == 3)
        {
            indexObj.number = @"2000";
            
        }
        else  if (number == 4)
        {
            indexObj.number = @"2000";
            
        }
        else
        {
            indexObj.number = @"500";
            
        }
      

        //是否免仓
        int storagetime = (int) [dic objectForKey:@"storagetime"];

        if (storagetime == 0) {
            
            indexObj.storagetime = @"五天";
            
        }else{
            
            indexObj.storagetime = @"七天";
        }
        
        
        //保证金
        int cautionmoney = (int) [dic objectForKey:@"cautionmoney"];
        
        if (cautionmoney == 0) {
            
            indexObj.cautionmoney = @"0";
            
        }else if (spotfutures == 1)
        {
            indexObj.cautionmoney = @"5%";
            
        }else
        {
            indexObj.cautionmoney = @"10%";

        }
        
        //备注
        NSString *remarks = (NSString *) [dic objectForKey:@"remarks"];
        
        
        indexObj.remarks = remarks;
        
        //报盘人
        int offerpeople = (int)[dic objectForKey:@"offerpeople"];
        
        //发票
        int invoice = (int)[dic objectForKey:@"invoice"];
        
        if (invoice == 0) {
            
            indexObj.invoice = @"本月";
            
        }else
        {
            indexObj.invoice = @"下月";

        }
        
        //税
        int cargobonded = (int)[dic objectForKey:@"cargobonded"];
        
        
        if (cargobonded == 0) {
            
            indexObj.cargobonded = @"船税";
        }else
        {
            indexObj.cargobonded = @"保税";

        }
        
        //勿扰
        
        
        if ( [dic objectForKey:@"notfaze"] != [NSNull null]) {
            
            int  notfaze = [[dic objectForKey:@"notfaze"] intValue];
            
            if (notfaze == 0) {
                
                indexObj.notfaze = [NSString stringWithFormat:@"%d",notfaze];
                
            }else
            {
                indexObj.notfaze = [NSString stringWithFormat:@"%d",notfaze];
                
            }
            
        }
        

//
        [_dataInfo2 addObject:indexObj];
        
        [indexObj release];
        
    }

    
    DebugLog(@"offer_results == %@ == %d  _dataInfo2 == %@",offer_results,[offer_results count],_dataInfo2);
    
    
    [_tableView1 reloadData];
    
    [_tableView2 reloadData];
    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    DebugLog(@"requestFailedrequestFailed");
    
    
    
}

#pragma mark - editItem
-(void)editButtonClick
{
    
    DebugLog(@"编辑");
    
}

#pragma mark - dateFromString


- (NSString *)date:(NSDate *)aDate
{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    
    //zzz表示时区，zzz可以删除，这样返回的日期字符将不包含时区信息。
    
    [dateFormatter setDateFormat:@"MM/dd HH:mm zzz"];
    
    
    NSString *destDateString = [dateFormatter stringFromDate:aDate];
    
    
    [dateFormatter release];
    
    
    NSString *auditingDate = [destDateString substringToIndex:[destDateString  length] - 9];
    
    return auditingDate;
}


- (NSString *)deliveryDate:(NSDate *)aDate
{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    
    //zzz表示时区，zzz可以删除，这样返回的日期字符将不包含时区信息。
    
    [dateFormatter setDateFormat:@"MM/dd zzz"];
    
    
    NSString *destDateString = [dateFormatter stringFromDate:aDate];
    
    
    [dateFormatter release];
    
    
    NSString *auditingDate = [destDateString substringToIndex:[destDateString  length] - 5];
    
    return auditingDate;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
