//
//  kata_IndexViewController.h
//  LargeClan
//
//  Created by kata on 13-11-19.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "kata_segView.h"
#import "ASIHTTPRequest.h"
@interface kata_IndexViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,kata_segViewDelegate,
    ASIHTTPRequestDelegate>



@end
