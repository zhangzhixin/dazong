//
//  kata_UITabBarController.h
//  CityStar
//
//  Created by 黎 斌 on 12-8-2.
//  Copyright (c) 2012年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol KATAUITabBarControllerDelegate;

@interface KATAUITabBarController : UITabBarController
{
	int currentSelectedIndex;
	UIImageView * selectedItemBg;
    
}



@property (nonatomic, assign) int currentSelectedIndex;
@property (nonatomic, assign) int lastSelectedIndex;

@property (nonatomic, assign) id<KATAUITabBarControllerDelegate> kataTabBarControllerDelegate;


- (void)selectTabIdx:(NSInteger)index;


- (void)showTabBar;

- (void)hideTabBar;

@end

@protocol KATAUITabBarControllerDelegate <NSObject>

@required

- (NSString *)imageNameForTabBarController:(KATAUITabBarController *)conteroller buttonState:(UIControlState)state itemIndex:(int)index;

- (NSString *)titleForTabBarController:(KATAUITabBarController *)controller itemIndex:(int)index;

- (NSString *)imageNameForSelectedTabBarItem;

- (NSString *)imageNameForCustomTabBarBg;


@end