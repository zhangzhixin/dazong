//
//  kata_UITabBarButtonItem.m
//  CityStar
//
//  Created by 黎 斌 on 12-8-3.
//  Copyright (c) 2012年 kata. All rights reserved.
//

#import "KATAUITabBarButtonItem.h"

@implementation KATAUITabBarButtonItem
@synthesize tabBarItemTitle;

- (void)dealloc
{
	self.tabBarItemTitle = nil;
	
	[super dealloc];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
		UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 32, self.frame.size.width, self.frame.size.height - 32)];
		lbl.backgroundColor = [UIColor clearColor];
        
		lbl.textAlignment = UITextAlignmentCenter;
        
		self.tabBarItemTitle = lbl;
        
		[lbl release];
		
		[self addSubview:lbl];
		
    }
    return self;
}

- (void)setFrame:(CGRect)frame
{
	[super setFrame:frame];
	
	self.tabBarItemTitle.frame = CGRectMake(0, 32, self.frame.size.width, self.frame.size.height - 32);
}

- (void)setTitle:(NSString *)title forState:(UIControlState)state
{
	self.tabBarItemTitle.text = title;
}

- (void)setTitleColor:(UIColor *)color forState:(UIControlState)state
{
	[super setTitleColor:color forState:state];
	if (state == UIControlStateNormal) {
		self.tabBarItemTitle.textColor = color;
	}
}

- (void)setFont:(UIFont *)font
{
	self.tabBarItemTitle.font = font;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
