//
//  kata_UITabBarController.m
//  CityStar
//
//  Created by 黎 斌 on 12-8-2.
//  Copyright (c) 2012年 kata. All rights reserved.
//
//动画持续时间，该时间与压栈和出栈时间相当
#define SLIDE_ANIMATION_DURATION 0.35


#import "KATAUITabBarController.h"
#import "KATAUITabBarButtonItem.h"
#import <QuartzCore/QuartzCore.h>

@interface KATAUITabBarController ()
{
    UIView * _customTabBarBg;
}
@property (nonatomic, retain) NSMutableArray * buttons;

- (void)hideRealTabBar;
- (void)createCustomTabBar;

- (void)selectedTab:(UIButton *)button;

- (void)moveBgToPosition:(NSNumber *)leftPos;

@end

@implementation KATAUITabBarController

@synthesize currentSelectedIndex;
@synthesize buttons;

@synthesize kataTabBarControllerDelegate;

- (void)dealloc
{
	self.buttons = nil;
	[super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
//        [self.view setBackgroundColor:[UIColor blackColor]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	if (!buttons) {
		[self hideRealTabBar];
		[self createCustomTabBar];
	}
    

}

#pragma mark - 自定义tab bar
- (void)hideRealTabBar
{
	for(UIView * subview in self.view.subviews)
	{
		if([subview isKindOfClass:[UITabBar class]])
		{
			subview.hidden = YES;
			break;
		}
	}
    
//    [KATADebug printClassName:self.view];
}
- (void)createCustomTabBar
{
	
//	自定义tabbar背景
	_customTabBarBg = [[UIView alloc] initWithFrame:CGRectMake(0, self.tabBar.frame.origin.y, self.view.frame.size.width, self.tabBar.frame.size.height)];
	NSString * customTabBarImageName = [self.kataTabBarControllerDelegate imageNameForCustomTabBarBg];
    
	_customTabBarBg.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:customTabBarImageName]];
    _customTabBarBg.tag = 89;
	[self.view addSubview:_customTabBarBg];
	
//	[_customTabBarBg release];
	
//	创建按钮和选中项背景
	int tabsCount = self.viewControllers.count > 4? 4 : [self.viewControllers count];
	
	self.buttons = [NSMutableArray arrayWithCapacity:tabsCount];
	
//	float __width = floor(self.view.frame.size.width / tabsCount);
//	float __height = self.tabBar.frame.size.height;
    
    float __width = floor(self.view.frame.size.width / tabsCount);
	float __height = self.tabBar.frame.size.height;

//	选中项背景
	NSString * customTabBarSelectedImageName = [self.kataTabBarControllerDelegate imageNameForSelectedTabBarItem];
	selectedItemBg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:customTabBarSelectedImageName]];
	selectedItemBg.contentMode = UIViewContentModeScaleToFill;
	selectedItemBg.frame = CGRectMake(-__width, 0, __width , __height);
	[_customTabBarBg addSubview:selectedItemBg];
	
//	按钮
	for (int i = 0; i < tabsCount; i++) {
		KATAUITabBarButtonItem * btn = [KATAUITabBarButtonItem buttonWithType:UIButtonTypeCustom];
		btn.frame = CGRectMake(i * __width, 0, __width, __height);
        
        btn.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        
		[btn addTarget:self action:@selector(selectedTab:) forControlEvents:UIControlEventTouchUpInside];
		
        btn.tag = i;
		
        
//        UIImageView *iamgeView = [[UIImageView alloc]init];
        
		NSString * normalStateImageName = [self.kataTabBarControllerDelegate imageNameForTabBarController:self buttonState:UIControlStateNormal itemIndex:i];
		NSString * highlightedStateImageName = [self.kataTabBarControllerDelegate imageNameForTabBarController:self buttonState:UIControlStateHighlighted itemIndex:i];
		NSString * tabTitle = [self.kataTabBarControllerDelegate titleForTabBarController:self itemIndex:i];
		
//		设置图片
		[btn setImage:[UIImage imageNamed:normalStateImageName] forState:UIControlStateNormal];
		[btn setImage:[UIImage imageNamed:highlightedStateImageName] forState:UIControlStateHighlighted];
		[btn setImage:[UIImage imageNamed:highlightedStateImageName] forState:UIControlStateSelected];
        
//        btn.backgroundColor = [UIColor greenColor];
        
//        [btn setTitleEdgeInsets:UIEdgeInsetsMake( 0,0, -17,0)];
    
		[btn setImageEdgeInsets:UIEdgeInsetsMake(-17, 0, 0, 0)];
        
        
		
//		设置标题
		[btn setTitle:tabTitle forState:UIControlStateNormal];
		[btn setTitleColor:[UIColor colorWithWhite:0.45 alpha:1] forState:UIControlStateNormal];
		[btn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
		[btn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
		[btn setFont:[UIFont boldSystemFontOfSize:11]];

		[self.buttons addObject:btn];
        
		[_customTabBarBg addSubview:btn];
        
	}
	
	[self selectedTab:[self.buttons objectAtIndex:0]];
}

- (void)selectTabIdx:(NSInteger)index
{
    [self selectedTab:[self.buttons objectAtIndex:index]];
}

- (void)selectedTab:(UIButton *)button
{
	if (button.tag == self.currentSelectedIndex && [[self.viewControllers objectAtIndex:button.tag] isKindOfClass:[UINavigationController class]]) {
		[[self.viewControllers objectAtIndex:button.tag] popToRootViewControllerAnimated:YES];
	}
	
	for (KATAUITabBarButtonItem * btnItem in self.buttons) {
		if (btnItem == button) {
			btnItem.selected = YES;
            
            self.lastSelectedIndex = self.currentSelectedIndex;
            
			self.currentSelectedIndex = btnItem.tag;
			self.selectedIndex = self.currentSelectedIndex;
			btnItem.highlighted = btnItem.selected?NO:YES;
			
			float leftPos = btnItem.frame.size.width * btnItem.tag;
			
			[self performSelector:@selector(moveBgToPosition:) withObject:[NSNumber numberWithFloat:leftPos]];
			
		}else {
			btnItem.selected = NO;
			btnItem.highlighted = NO;
		}
	}
}



- (void)showTabBar
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.35];
    _customTabBarBg.frame = CGRectMake(0, self.tabBar.frame.origin.y, self.view.frame.size.width, self.tabBar.frame.size.height);
    [UIView commitAnimations];
    NSLog(@"显示TabBar");
}

- (void)hideTabBar
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.35];
    _customTabBarBg.frame = CGRectMake(-self.view.frame.size.width, self.tabBar.frame.origin.y, self.view.frame.size.width, self.tabBar.frame.size.height);
    [UIView commitAnimations];
    NSLog(@"隐藏TabBar");
}


- (void)moveBgToPosition:(NSNumber *)leftPos
{
	float leftPosFloat = [leftPos floatValue];
	CGRect targetFrame = CGRectMake(leftPosFloat, 0, selectedItemBg.frame.size.width, selectedItemBg.frame.size.height);
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.15];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
	selectedItemBg.frame = targetFrame;
	[UIView commitAnimations];
}

@end
