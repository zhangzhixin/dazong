//
//  kata_DeliveryTimeCell.m
//  LargeClan
//
//  Created by kata on 13-12-28.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import "kata_DeliveryTimeCell.h"

@interface kata_DeliveryTimeCell ()



@property(nonatomic,retain)UILabel *nameLbl1;

@property(nonatomic,retain)UITextField *timeTxt1;

@property(nonatomic,retain)UILabel *nameLbl2;


@property(nonatomic,retain)UITextField *timeTxt2;

@end


@implementation kata_DeliveryTimeCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        
        
       _nameLbl1 = [[UILabel alloc]initWithFrame:CGRectZero];
        
        _nameLbl1.text = @"交割时间";
        
        _nameLbl1.backgroundColor = [UIColor redColor];
        
        _nameLbl1.tag = 11;
        
        [self addSubview:_nameLbl1];
        
        
        
        _nameLbl2 = [[UILabel alloc]initWithFrame:CGRectZero];
        
        _nameLbl2.text = @"至";
        
        _nameLbl2.backgroundColor = [UIColor blueColor];

        
        [self addSubview:_nameLbl2];
        
    
        _timeTxt1 = [[UITextField alloc]initWithFrame:CGRectZero];
        
        _timeTxt1.backgroundColor = [UIColor greenColor];

        _timeTxt1.delegate = self;
        
         [self addSubview:_timeTxt1];

        
        _timeTxt2 = [[UITextField alloc]initWithFrame:CGRectZero];
        
        _timeTxt2.backgroundColor = [UIColor orangeColor];

        
        _timeTxt2.delegate = self;
        
        [self addSubview:_timeTxt2];

        [self layoutSubviews];
        
        
    }
    return self;
}

- (void)dealloc
{
    
    [_nameLbl1 release];
    
    [_timeTxt1 release];
    
    [_nameLbl2 release];
    
    [_timeTxt2 release];

    
    [super dealloc];
}

- (void)layoutSubviews
{
    
    _nameLbl1.frame = CGRectMake(10, 0, 80, 44);
    
    
    _timeTxt1.frame = CGRectMake(_nameLbl1.frame.origin.x +_nameLbl1.frame.size.width, 0, 90, 44);
    
    
    _nameLbl2.frame = CGRectMake( _timeTxt1.frame.origin.x + _timeTxt1.frame.size.width, 0, 40, 44);

    
    _timeTxt2.frame = CGRectMake(_nameLbl2.frame.origin.x +_nameLbl2.frame.size.width, 0, 90, 44);

    
    
}

#pragma mark - textFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyWillPresent) name:UIKeyboardWillShowNotification object:nil];
    
}

- (void)keyWillPresent
{
    
    UIWindow *tempWindow = [[[UIApplication sharedApplication] windows] objectAtIndex:1];
    
    tempWindow.alpha = 0;
    
}


@end
