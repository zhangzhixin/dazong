//
//  kata_VerificationRequest.h
//  LargeClan
//
//  Created by kata on 13-12-19.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import "kata_BaseRequest.h"

@interface kata_VerificationRequest : kata_BaseRequest


@property (strong, nonatomic) NSMutableDictionary * params;

@property (strong, nonatomic) NSMutableString * paramsStr;


- (NSString *)method;

- (NSString *)url;


- (id)initWithHandPhone:(NSString *)handPhone;

@end
