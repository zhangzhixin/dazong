//
//  kata_VerificationRequest.m
//  LargeClan
//
//  Created by kata on 13-12-19.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import "kata_VerificationRequest.h"

#import "KATAConstants.h"

#import "NSString+MD5.h"

@implementation kata_VerificationRequest
- (void)dealloc
{
    self.params = nil;
    [super dealloc];
}

- (id)initWithHandPhone:(NSString *)handPhone
{
    self = [super init];
    if (self) {
        
        
        NSString *info = @"1";
        
        NSMutableString *params  = [NSMutableString stringWithFormat:@"{handphone:%@ info:%@}",handPhone,info];
        
        self.paramsStr  = params ;
        
    }
    return self;
}

- (NSString *)sign
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    NSMutableString *sign = [NSMutableString stringWithString:@"1001 +"];
    
    [sign appendFormat:@"%@ + %ld",[self method],timestamp];
    
    [sign MD5];
    
    return [sign description] ;
}


- (NSString *)appkey
{
    return @"10010";
    
}
- (NSString *)method
{
    return @"user_sendVerification_update";
}

- (NSString *)params
{
    return self.paramsStr;
}

- (NSString *)url
{
    

    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    NSString * urlStr = [NSString stringWithFormat:@"%@?%@&time=%ld&method=%@&params=%@&sign=%@", SERVER_URI,[self appkey], timestamp,[self method], [self params],[self sign]];
    
    return urlStr;
}






@end
