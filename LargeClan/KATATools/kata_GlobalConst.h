//
//  kata_GlobalConst.h
//  CityStar
//
//  Created by 黎 斌 on 12-8-3.
//  Copyright (c) 2012年 kata. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface kata_GlobalConst : NSObject

extern NSString * const MODE;

//extern NSString * const MAIN_NAV_BG_NAME;
//extern NSString * const MAIN_TAB_BAR_BG;
//extern NSString * const MAIN_TAB_BAR_BG_SELECTED;
//
//extern NSString * const DEFAULT_LOCATION;
//extern NSString * const DEFAULT_LOCATION_ID;
//
//extern NSString * const CLLOCATION_KEY;
//extern NSString * const SELECTED_LOCATION_KEY;
//
//extern NSString * const SERVER_URI;
//
//extern NSString * const TEST_SERVER_URI;
//
//extern NSString * const REGIST;
//extern NSString * const LOGIN;
//
//extern NSString * const GET_MOBILE_CITY;
//extern NSString * const GET_INDEX_INFO;
//
//extern NSString * const UPLOAD_MOBILE_NEWS;
//extern NSString * const COMMENT_MOBILE_NEWS;
//extern NSString * const GET_MOBILE_NEWS;
//
//extern NSString * const GET_MOBILE_CLASS;
//extern NSString * const GET_MOBILE_SEARCH;
//extern NSString * const GET_MOBILE_SKU_BY_CLASS;
//extern NSString * const GET_MOBILE_SKU_INFO;
//
//extern NSString * const GET_MOBILE_CART;
//extern NSString * const UPDATE_MOBILE_CART;
//extern NSString * const UPDATE_MOBILE_ORDER;
//
//extern NSString * const GET_MOBILE_ACTIVITYS;
//extern NSString * const GET_MOBILE_ACTIVITY_INFO;
//extern NSString * const ATTEND_MOBILE_ACTIVITY;
//extern NSString * const COMMENT_MOBILE_ACTIVITY;
//
//extern NSString * const GET_MOBILE_SERVER;
//extern NSString * const GET_MOBILE_SERVER_INFO;
//extern NSString * const REQUEST_MOBILE_SERVER;
//
//extern NSString * const UPDATE_MOBILE_FEEDBACK;
//extern NSString * const GET_MOBILE_FAVORITES;
//extern NSString * const UPDATE_MOBILE_FAVORITE;
//
//extern NSString * const GET_TOP_CATEGORY;
//
//extern NSString * const GET_MOBILE_USER_INFO;
//
//extern NSString * const GET_MOBILE_ORDER_LIST;
//
//extern NSString * const GET_MOBILE_ORDER_INFO;
//
//extern NSString * const GET_MOBILE_ACTIVITY_APPLYS;
//
//extern NSString * const GET_MOBILE_ADDRESS;
//
//extern NSString * const UPDATE_MOBILE_ADDRESS;
//
//extern NSString * const CANCEL_MOBILE_ORDER;
//
//extern NSString * const GET_MOBILE_SUB_SERVER;

#define APPID @"4fb07786"
#define ENGINE_URL @"http://dev.voicecloud.cn:1028/index.htm"


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark add @2013/03/09

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//extern NSString * const TEST_SERVER_URI;
//extern NSString * const GET_NOTICE_DETAIL;
//extern NSString * const GET_SERVICE_ARTICLE;
//extern NSString * const GET_SUBPAGE_DATA;
//extern NSString * const GET_NEWS_SUB_CATEGORY_DATA;
//extern NSString * const GET_NEWS_SUB_LIST_DATA;
//extern NSString * const POST_HELP_INFO;
//extern NSString * const GET_MOBILE_EXPRESS;
//extern NSString * const DELETE_ADDRESS;
//extern NSString * const GET_MOBILE_SUB_CLASS;
//
//
//extern NSString * const APP_KEY;
//extern NSString * const APP_SECRET;
//
//extern NSString * const ENCODE_KEY;

@end
