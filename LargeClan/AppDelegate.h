//
//  AppDelegate.h
//  LargeClan
//
//  Created by kata on 13-11-19.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KATAUITabBarController.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate,KATAUITabBarControllerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) KATAUITabBarController * rootViewController;

@end
