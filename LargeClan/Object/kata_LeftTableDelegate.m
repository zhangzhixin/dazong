//
//  kata_LeftTableDelegate.m
//  LargeClan
//
//  Created by kata on 13-12-28.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import "kata_LeftTableDelegate.h"

@implementation kata_LeftTableDelegate



#pragma mark - rightTableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 44.0f;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *vi = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 140, 44)];
    
    vi.backgroundColor = [UIColor blueColor];
    
    UILabel *timeLbl = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, 60, 44)];
    
    timeLbl.text = @"报盘时间";
    

    timeLbl.adjustsFontSizeToFitWidth = YES;
    
    [vi addSubview:timeLbl];

    [timeLbl release];
    
    
    UILabel *numberLbl = [[UILabel alloc]initWithFrame:CGRectMake(70, 0, 60, 44)];
    
    numberLbl.text = @"报盘编号";
    
    numberLbl.adjustsFontSizeToFitWidth = YES;
    
    [vi addSubview:numberLbl];
    
    [numberLbl release];

    
    return vi;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static  NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        
        cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
        
        
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.textLabel.text = @"left";
    
    return cell;
    
    
    
}


@end
