//
//  kata_RightTableDelegate.m
//  LargeClan
//
//  Created by kata on 13-12-28.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#define LBLWIDTH 80


#import "kata_RightTableDelegate.h"

#import "KATAUtils.h"
@interface kata_RightTableDelegate ()


@property(nonatomic,retain)NSMutableArray *rightHeaderArr;
@end

@implementation kata_RightTableDelegate

- (id)init
{
    if (self = [super init]) {
        
        _rightHeaderArr  =  [[NSMutableArray alloc]initWithObjects:@"方向",@"人民币",@"货种",@"交割时间",@"交割地",@"保证金",@"数量（吨）",@"报盘人",@"发票",@"免仓",@"状态",@"备注", nil];


        
        
    }
    
    
    return self;
    
}
- (void)dealloc
{
    [_rightHeaderArr release];
    
    [super dealloc];
}
#pragma makr -  kata_segViewDelegate




#pragma mark - rightTableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 44.0f;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *vi = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 140, 44)];
    
    vi.backgroundColor = [UIColor blueColor];
    

    for (int i = 0; i < [_rightHeaderArr count]; i++) {
        
        
        UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(i * LBLWIDTH, 0, LBLWIDTH, 44)];
        
        lbl.text = [_rightHeaderArr objectAtIndex:i];
        
        lbl.textAlignment = NSTextAlignmentCenter;
        
        lbl.adjustsFontSizeToFitWidth = YES;
        
        [vi addSubview:lbl];
        
        [lbl release];
        
        
    }
    
    
    
    
    return vi;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static  NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        
        cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
        
        
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.textLabel.text = @"right";
    
    return cell;
    
    
    
}





@end
