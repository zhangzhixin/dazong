//
//  KATAIndex.h
//  LargeClan
//
//  Created by kata on 13-12-24.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KATAIndex : NSObject


@property (nonatomic,retain) NSString * code;

@property (nonatomic,retain) NSString * offertime;
@property (nonatomic,retain) NSString * offerstate;
@property (nonatomic,retain) NSString * auditingstatus;
@property (nonatomic,retain) NSString * isdeleted;
@property (nonatomic,retain) NSString * iscancel;
@property (nonatomic,retain) NSString * direction;
@property (nonatomic,retain) NSString * nameofpart;
@property (nonatomic,retain) NSString * number;
@property (nonatomic,retain) NSString * storagetime;
@property (nonatomic,retain) NSString * cautionmoney;
@property (nonatomic,retain) NSString * remarks;
@property (nonatomic,retain) NSString * offerpeople;
@property (nonatomic,retain) NSString * invoice;
@property (nonatomic,retain) NSString * cargobonded;
@property (nonatomic,retain) NSString * loadingtime1;
@property (nonatomic,retain) NSString * loadingtime2;
@property (nonatomic,retain) NSString * arrivetime1;
@property (nonatomic,retain) NSString * arrivetime2;
@property (nonatomic,retain) NSString * changehands;
@property (nonatomic,retain) NSString * surrenderdocuments;
@property (nonatomic,retain) NSString * detailedlist;
@property (nonatomic,retain) NSString * paymentmethods;
@property (nonatomic,retain) NSString * dispatching;
@property (nonatomic,retain) NSString * brand;
@property (nonatomic,retain) NSString * region;
@property (nonatomic,retain) NSString * goodsSource;
@property (nonatomic,retain) NSString * user;
@property (nonatomic,retain) NSString * notfaze;

@property (nonatomic,retain) NSString * spotfutures;

@property (nonatomic,retain) NSString * deliverytime1;
@property (nonatomic,retain) NSString * deliverytime2;
@property (nonatomic,retain) NSMutableString * deliverytime;

@property (nonatomic,retain) NSString * deliveryname;

@property (nonatomic,retain) NSString * quotation;

@property (nonatomic,retain) NSString * goodssourcename;

@property (nonatomic,retain) NSString * dealstatus;

@property (nonatomic,retain) NSString * state;



@end
