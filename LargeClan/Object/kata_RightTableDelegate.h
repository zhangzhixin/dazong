//
//  kata_RightTableDelegate.h
//  LargeClan
//
//  Created by kata on 13-12-28.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "kata_segView.h"

@interface kata_RightTableDelegate : NSObject <UITableViewDelegate,UITableViewDataSource,kata_segViewDelegate>

@end
